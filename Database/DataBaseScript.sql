
USE [3]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetExtraHoursByEmployee]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Juan David Maldonado>
-- Create date: <Create Date,,>
-- Description:	<Description, Trae las horas extra por Usuario,>
/*
	EXEC NominaV.dbo.sp_GetExtraHoursByEmployee 
		@IDEmpleado = 2,
		@Filter = 'N'
*/
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetExtraHoursByEmployee]
	@IDEmpleado INT,
	@Filter NVARCHAR(1) = 'N'
AS
BEGIN
	IF (@Filter = 'N')
	BEGIN 
		SELECT
			HE.IDReporteHE,
			HE.IDEmpleado,
			HE.Descripcion,
			HE.Estado,
			E.Nombres,
			E.Apellidos,
			PH.IDJefe IdPadre,
			E2.Nombres		AS NombresAprobador,
			E2.Apellidos	AS ApellidosAprobador,
			CASE WHEN C.Cantidad IS NULL
				THEN 0
				ELSE C.Cantidad END AS Cantidad
		FROM HorasExtra HE WITH (NOLOCK)
			INNER JOIN Empleados E			WITH (NOLOCK) ON E.IDEmpleado	= HE.IDEmpleado
			INNER JOIN Empleados PH		WITH (NOLOCK) ON E.IDEmpleado	= PH.IDEmpleado
			INNER JOIN Empleados E2			WITH (NOLOCK) ON E2.IDEmpleado  = PH.IDJefe
			LEFT JOIN  HorasExtrasCant C	WITH (NOLOCK) ON HE.IDReporteHE	= C.IdReporte
		WHERE HE.IDEmpleado = @IDEmpleado
	END
	ELSE
	BEGIN
		SELECT
			HE.IDReporteHE,
			HE.IDEmpleado,
			HE.Descripcion,
			HE.Estado,
			E.Nombres,
			E.Apellidos,
			PH.IDJefe IdPadre,
			E2.Nombres		AS NombresAprobador,
			E2.Apellidos	AS ApellidosAprobador,
			CASE WHEN C.Cantidad IS NULL
				THEN 0
				ELSE C.Cantidad END AS Cantidad
		FROM HorasExtra HE WITH (NOLOCK)
			INNER JOIN Empleados E			WITH (NOLOCK) ON E.IDEmpleado	= HE.IDEmpleado
			INNER JOIN Empleados PH		WITH (NOLOCK) ON E.IDEmpleado	= PH.IDEmpleado
			INNER JOIN Empleados E2			WITH (NOLOCK) ON E2.IDEmpleado  = PH.IDJefe
			LEFT JOIN  HorasExtrasCant C	WITH (NOLOCK) ON HE.IDReporteHE	= C.IdReporte
		WHERE	HE.IDEmpleado = @IDEmpleado
					AND HE.Estado = @Filter
	END	
	
END

GO
/****** Object:  Table [dbo].[Areas]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Areas](
	[IDArea] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDArea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleados](
	[IDEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[Cedula] [nvarchar](20) NOT NULL,
	[Nombres] [nvarchar](100) NOT NULL,
	[Apellidos] [nvarchar](100) NOT NULL,
	[FechaIngreso] [date] NOT NULL,
	[IDJefe] [int] NULL,
	[Cargo] [nvarchar](100) NOT NULL,
	[IDArea] [int] NULL,
	[Telefono] [nvarchar](30) NULL,
	[Movil] [nvarchar](30) NULL,
	[Email] [nvarchar](30) NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDHorario] [int] NULL,
	[Aprobador] [bit] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Genero] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK__Empleado__50621DCD0DAF0CB0] PRIMARY KEY CLUSTERED 
(
	[IDEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[IDEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[NIT] [nvarchar](9) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IDEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Festivos]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Festivos](
	[Fecha] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoraExtraDetalle]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoraExtraDetalle](
	[IDHoraExtraDetalle] [int] IDENTITY(1,1) NOT NULL,
	[IDReporteHE] [int] NOT NULL,
	[FechaEntrada] [datetime] NOT NULL,
	[FechaSalida] [datetime] NOT NULL,
	[Observaciones] [nvarchar](max) NULL,
	[Estado] [nvarchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDHoraExtraDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Horarios]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horarios](
	[IDHorario] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](max) NOT NULL,
	[LunesEntrada] [time](7) NULL,
	[LunesSalida] [time](7) NULL,
	[MartesEntrada] [time](7) NULL,
	[MartesSalida] [time](7) NULL,
	[MiercolesEntrada] [time](7) NULL,
	[MiercolesSalida] [time](7) NULL,
	[JuevesEntrada] [time](7) NULL,
	[JuevesSalida] [time](7) NULL,
	[ViernesEntrada] [time](7) NULL,
	[ViernesSalida] [time](7) NULL,
	[SabadoEntrada] [time](7) NULL,
	[SabadoSalida] [time](7) NULL,
	[DomingoEntrada] [time](7) NULL,
	[DomingoSalida] [time](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[IDHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HorasExtra]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HorasExtra](
	[IDReporteHE] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpleado] [int] NOT NULL,
	[Descripcion] [nvarchar](max) NOT NULL,
	[Estado] [nvarchar](1) NOT NULL,
	[Fecha] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDReporteHE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[HorasExtrasCant]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[HorasExtrasCant] AS
SELECT
	IDReporteHE	AS IdReporte,
	COUNT(*)	AS Cantidad
FROM HoraExtraDetalle WITH (NOLOCK)
GROUP BY IDReporteHE
GO
/****** Object:  View [dbo].[PlantillasPorEmpleado]    Script Date: 14/09/2015 08:08:03 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PlantillasPorEmpleado]
AS
SELECT        HE.IDEmpleado, CASE WHEN A.Aprobadas IS NULL THEN 0 ELSE A.Aprobadas END AS Aprobadas, CASE WHEN S.SinAprobar IS NULL 
                         THEN 0 ELSE S.SinAprobar END AS SinAprobar, CASE WHEN L.Liquidadas IS NULL THEN 0 ELSE L.Liquidadas END AS Liquidadas, 
                         CASE WHEN R.Rechazadas IS NULL THEN 0 ELSE R.Rechazadas END AS Rechazadas, COUNT(*) AS Total
FROM            dbo.HorasExtra AS HE WITH (NOLOCK) LEFT OUTER JOIN
                             (SELECT        IDEmpleado, COUNT(*) AS Aprobadas
                               FROM            dbo.HorasExtra WITH (NOLOCK)
                               WHERE        (Estado = 'A')
                               GROUP BY IDEmpleado) AS A ON A.IDEmpleado = HE.IDEmpleado LEFT OUTER JOIN
                             (SELECT        IDEmpleado, COUNT(*) AS SinAprobar
                               FROM            dbo.HorasExtra AS HorasExtra_3 WITH (NOLOCK)
                               WHERE        (Estado = 'S')
                               GROUP BY IDEmpleado) AS S ON S.IDEmpleado = HE.IDEmpleado LEFT OUTER JOIN
                             (SELECT        IDEmpleado, COUNT(*) AS Liquidadas
                               FROM            dbo.HorasExtra AS HorasExtra_2 WITH (NOLOCK)
                               WHERE        (Estado = 'L')
                               GROUP BY IDEmpleado) AS L ON L.IDEmpleado = HE.IDEmpleado LEFT OUTER JOIN
                             (SELECT        IDEmpleado, COUNT(*) AS Rechazadas
                               FROM            dbo.HorasExtra AS HorasExtra_1 WITH (NOLOCK)
                               WHERE        (Estado = 'R')
                               GROUP BY IDEmpleado) AS R ON R.IDEmpleado = HE.IDEmpleado
GROUP BY HE.IDEmpleado, A.Aprobadas, S.SinAprobar, L.Liquidadas, R.Rechazadas

GO
SET IDENTITY_INSERT [dbo].[Areas] ON 

GO
INSERT [dbo].[Areas] ([IDArea], [Nombre]) VALUES (1, N'Sistemas')
GO
INSERT [dbo].[Areas] ([IDArea], [Nombre]) VALUES (2, N'Contabilidad')
GO
SET IDENTITY_INSERT [dbo].[Areas] OFF
GO
SET IDENTITY_INSERT [dbo].[Empleados] ON 

GO
INSERT [dbo].[Empleados] ([IDEmpleado], [Cedula], [Nombres], [Apellidos], [FechaIngreso], [IDJefe], [Cargo], [IDArea], [Telefono], [Movil], [Email], [IDEmpresa], [IDHorario], [Aprobador], [Estado], [Genero]) VALUES (2, N'1022973561', N'Juan David', N'Maldonado G�mez', CAST(0xFC390B00 AS Date), 4, N'Desarrollador de Software', 1, N'3881807', N'3046504259', N'maldo.s@hotmail.com', 2, 2, 1, 1, N'M')
GO
INSERT [dbo].[Empleados] ([IDEmpleado], [Cedula], [Nombres], [Apellidos], [FechaIngreso], [IDJefe], [Cargo], [IDArea], [Telefono], [Movil], [Email], [IDEmpresa], [IDHorario], [Aprobador], [Estado], [Genero]) VALUES (3, N'1022994408', N'Cristian Eduardo', N'Maldonado G�mez', CAST(0xE6390B00 AS Date), 4, N'Dise�ador Gr�fico', 2, N'321654987', N'301 45 6789', N'cemaldonado@gmail.com', 1, 1, 0, 1, N'M')
GO
INSERT [dbo].[Empleados] ([IDEmpleado], [Cedula], [Nombres], [Apellidos], [FechaIngreso], [IDJefe], [Cargo], [IDArea], [Telefono], [Movil], [Email], [IDEmpresa], [IDHorario], [Aprobador], [Estado], [Genero]) VALUES (4, N'51905148', N'Irma', N'G�mez Pedreros', CAST(0x153A0B00 AS Date), NULL, N'Auxiliar Contable', 2, N'3881807', N'3124875358', N'igp140@hotmail.com', 1, 3, 1, 1, N'F')
GO
INSERT [dbo].[Empleados] ([IDEmpleado], [Cedula], [Nombres], [Apellidos], [FechaIngreso], [IDJefe], [Cargo], [IDArea], [Telefono], [Movil], [Email], [IDEmpresa], [IDHorario], [Aprobador], [Estado], [Genero]) VALUES (5, N'1023868255', N'Andr�s Mauricio', N'Maldonado G�mez', CAST(0x233A0B00 AS Date), 4, N'Asesor Comercial', 1, N'8989898', N'3002226677', N'otromail@mail.co', 1, 6, 0, 1, N'M')
GO
INSERT [dbo].[Empleados] ([IDEmpleado], [Cedula], [Nombres], [Apellidos], [FechaIngreso], [IDJefe], [Cargo], [IDArea], [Telefono], [Movil], [Email], [IDEmpresa], [IDHorario], [Aprobador], [Estado], [Genero]) VALUES (6, N'1122334455', N'Juliana', N'Mu�oz', CAST(0xE5390B00 AS Date), 4, N'Auxiliar de Servicios Generales', 2, N'33322245', N'9988774455', NULL, 1, 1, 0, 1, N'F')
GO
SET IDENTITY_INSERT [dbo].[Empleados] OFF
GO
SET IDENTITY_INSERT [dbo].[Empresas] ON 

GO
INSERT [dbo].[Empresas] ([IDEmpresa], [NIT], [Nombre]) VALUES (1, N'1022', N'Juan S.A.')
GO
INSERT [dbo].[Empresas] ([IDEmpresa], [NIT], [Nombre]) VALUES (2, N'1234', N'Inversiones PEPITO')
GO
SET IDENTITY_INSERT [dbo].[Empresas] OFF
GO
SET IDENTITY_INSERT [dbo].[HoraExtraDetalle] ON 

GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (2, 1, CAST(0x0000A4BD0062E080 AS DateTime), CAST(0x0000A4BD015A11C0 AS DateTime), N'Mantenimiento Servidores', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (3, 1, CAST(0x0000A4BE0062E080 AS DateTime), CAST(0x0000A4BE015A11C0 AS DateTime), N'Mantenimiento Servidores', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (6, 4, CAST(0x0000A4C8005265C0 AS DateTime), CAST(0x0000A4C80179A7B0 AS DateTime), N'3 horas Nocturnas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (7, 5, CAST(0x0000A4C7002932E0 AS DateTime), CAST(0x0000A4C7018A2270 AS DateTime), N'limpieza Salas de Juntas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (12, 6, CAST(0x0000A5150083D600 AS DateTime), CAST(0x0000A515013C21B0 AS DateTime), N'asasasas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (13, 6, CAST(0x0000A51B0083D600 AS DateTime), CAST(0x0000A51B013C21B0 AS DateTime), N'asasasas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (14, 6, CAST(0x0000A50D0083D600 AS DateTime), CAST(0x0000A50D013C21B0 AS DateTime), N'asasasas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (15, 6, CAST(0x0000A5080083D600 AS DateTime), CAST(0x0000A508013C21B0 AS DateTime), N'asasasas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (16, 6, CAST(0x0000A5170083D600 AS DateTime), CAST(0x0000A517013C21B0 AS DateTime), N'asasasas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (17, 5, CAST(0x0000A5060011DA50 AS DateTime), CAST(0x0000A50600644010 AS DateTime), N'ddddddddddddddddd', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (18, 5, CAST(0x0000A5070011DA50 AS DateTime), CAST(0x0000A50700644010 AS DateTime), N'ddddddddddddddddd', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (19, 5, CAST(0x0000A5080011DA50 AS DateTime), CAST(0x0000A50800644010 AS DateTime), N'ddddddddddddddddd', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (20, 5, CAST(0x0000A5090011DA50 AS DateTime), CAST(0x0000A50900644010 AS DateTime), N'ddddddddddddddddd', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (21, 5, CAST(0x0000A50A0011DA50 AS DateTime), CAST(0x0000A50A00644010 AS DateTime), N'ddddddddddddddddd', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (22, 7, CAST(0x0000A50601391C40 AS DateTime), CAST(0x0000A5070062E080 AS DateTime), N'test horas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (23, 7, CAST(0x0000A50701391C40 AS DateTime), CAST(0x0000A5080062E080 AS DateTime), N'test horas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (24, 7, CAST(0x0000A50801391C40 AS DateTime), CAST(0x0000A5090062E080 AS DateTime), N'test horas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (25, 7, CAST(0x0000A50901391C40 AS DateTime), CAST(0x0000A50A0062E080 AS DateTime), N'test horas', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (26, 8, CAST(0x0000A50700E6B680 AS DateTime), CAST(0x0000A50800107AC0 AS DateTime), N'test normal', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (27, 8, CAST(0x0000A50800E6B680 AS DateTime), CAST(0x0000A50900107AC0 AS DateTime), N'test normal', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (28, 8, CAST(0x0000A50900E6B680 AS DateTime), CAST(0x0000A50A00107AC0 AS DateTime), N'test normal', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (29, 8, CAST(0x0000A50600E6B680 AS DateTime), CAST(0x0000A50700107AC0 AS DateTime), N'test normal', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (30, 8, CAST(0x0000A50701499700 AS DateTime), CAST(0x0000A508005265C0 AS DateTime), N'reerer', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (31, 8, CAST(0x0000A50801499700 AS DateTime), CAST(0x0000A509005265C0 AS DateTime), N'reerer', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (32, 8, CAST(0x0000A50901499700 AS DateTime), CAST(0x0000A50A005265C0 AS DateTime), N'reerer', NULL)
GO
INSERT [dbo].[HoraExtraDetalle] ([IDHoraExtraDetalle], [IDReporteHE], [FechaEntrada], [FechaSalida], [Observaciones], [Estado]) VALUES (33, 8, CAST(0x0000A50601499700 AS DateTime), CAST(0x0000A507005265C0 AS DateTime), N'reerer', NULL)
GO
SET IDENTITY_INSERT [dbo].[HoraExtraDetalle] OFF
GO
SET IDENTITY_INSERT [dbo].[Horarios] ON 

GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (1, N'7 a 5 Descanso Domingo', CAST(0x0700D85EAC3A0000 AS Time), CAST(0x0700E80A7E8E0000 AS Time), CAST(0x0700D85EAC3A0000 AS Time), CAST(0x0700E80A7E8E0000 AS Time), CAST(0x0700D85EAC3A0000 AS Time), CAST(0x0700E80A7E8E0000 AS Time), CAST(0x0700D85EAC3A0000 AS Time), CAST(0x0700E80A7E8E0000 AS Time), CAST(0x0700D85EAC3A0000 AS Time), CAST(0x0700E80A7E8E0000 AS Time), CAST(0x070040230E430000 AS Time), CAST(0x070048F9F66C0000 AS Time), NULL, NULL)
GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (2, N'6 a 2 Descanso Viernes', CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time), NULL, NULL, CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x0700B0BD58750000 AS Time))
GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (3, N'6 a 5:30 Descanso S�bado', CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), CAST(0x0700709A4A320000 AS Time), CAST(0x07001CEDAE920000 AS Time))
GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (4, N'5 a 4 Descanso S�bado', CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), NULL, NULL, CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time))
GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (5, N'5 a 4 Descanso Viernes', CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), NULL, NULL, CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time), CAST(0x070008D6E8290000 AS Time), CAST(0x070080461C860000 AS Time))
GO
INSERT [dbo].[Horarios] ([IDHorario], [Descripcion], [LunesEntrada], [LunesSalida], [MartesEntrada], [MartesSalida], [MiercolesEntrada], [MiercolesSalida], [JuevesEntrada], [JuevesSalida], [ViernesEntrada], [ViernesSalida], [SabadoEntrada], [SabadoSalida], [DomingoEntrada], [DomingoSalida]) VALUES (6, N'8 a 6 descanso Domingo', CAST(0x070040230E430000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(0x070040230E430000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(0x0700A8E76F4B0000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(0x070040230E430000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(0x070040230E430000 AS Time), CAST(0x070050CFDF960000 AS Time), CAST(0x070040230E430000 AS Time), CAST(0x070050CFDF960000 AS Time), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Horarios] OFF
GO
SET IDENTITY_INSERT [dbo].[HorasExtra] ON 

GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (1, 2, N'Hora Extra Ejemplo # 1', N'S', CAST(0x0000A4BA00189085 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (2, 2, N'Hora Extra Ejemplo # 2', N'S', CAST(0x0000A4BA00189085 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (3, 2, N'Hora Extra Ejemplo # 3', N'S', CAST(0x0000A4BA00189085 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (4, 5, N'Agosto', N'S', CAST(0x0000A4C800000000 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (5, 6, N'Horas Extra Abril', N'S', CAST(0x0000A4C800000000 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (6, 6, N'Septiembre', N'S', CAST(0x0000A50F00000000 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (7, 6, N'Test', N'S', CAST(0x0000A51300000000 AS DateTime))
GO
INSERT [dbo].[HorasExtra] ([IDReporteHE], [IDEmpleado], [Descripcion], [Estado], [Fecha]) VALUES (8, 6, N'TEst', N'S', CAST(0x0000A51300000000 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[HorasExtra] OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Empleado__B4ADFE38108B795B]    Script Date: 14/09/2015 08:08:03 p.m. ******/
ALTER TABLE [dbo].[Empleados] ADD  CONSTRAINT [UQ__Empleado__B4ADFE38108B795B] UNIQUE NONCLUSTERED 
(
	[Cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Empresas__C7DEC3C2023D5A04]    Script Date: 14/09/2015 08:08:03 p.m. ******/
ALTER TABLE [dbo].[Empresas] ADD UNIQUE NONCLUSTERED 
(
	[NIT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HorasExtra] ADD  DEFAULT (getdate()) FOR [Fecha]
GO
ALTER TABLE [dbo].[HoraExtraDetalle]  WITH NOCHECK ADD FOREIGN KEY([IDReporteHE])
REFERENCES [dbo].[HorasExtra] ([IDReporteHE])
GO
