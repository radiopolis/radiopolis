payRollApp.controller('manageAreas_Ctrl', ['$scope', 'Areas', 'Notifications', function ($scope, Areas, Notifications) {

	$scope.filterOptions = {
        filterText: ''
    };
    $scope.newArea = {};

    var GetAreas = function () {
    	$scope.areas = Areas.Area.GetAreas({});
    };
	
	GetAreas();

	$scope.gridOptions = {
        data: 'areas',
        enableColumnResize: true,
        filterOptions: $scope.filterOptions,
        columnDefs: [
                    { field: 'iDArea', displayName: 'Id Area'},
                    { field: 'nombre', displayName: 'Descripción' }
                    ]
    };

    $scope.Save = function () {
    	Areas.add.AddArea({}, $scope.newArea).$promise.then(function () {
    		$scope.newArea = {};
    		GetAreas();
    	});
    };
}]);