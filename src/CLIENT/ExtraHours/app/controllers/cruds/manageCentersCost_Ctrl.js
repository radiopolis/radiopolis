payRollApp.controller('manageCentersCost_Ctrl', ['$scope', 'CentersCost', 'Notifications', function ($scope, CentersCost, Notifications) {

	$scope.filterOptions = {
        filterText: ''
    };
    $scope.newCenterCost = {};

    var GetCentersCost = function () {
    	$scope.centersCost = CentersCost.centerCost.GetCentersCost({});
        console.log($scope.centersCost);
    };
	
	GetCentersCost();

	$scope.gridOptions = {
        data: 'centersCost',
        enableColumnResize: true,
        filterOptions: $scope.filterOptions,
        columnDefs: [
                    { field: 'iDCentroCosto', displayName: 'Id Centro de Costo'},
                    { field: 'nombre', displayName: 'Descripción' }
                    ]
    };

    $scope.Save = function () {
    	CentersCost.addCenterCost.Add({}, $scope.newCenterCost).$promise.then(function () {
    		$scope.newCenterCost = {};
    		GetCentersCost();
    	});
    };
}]);