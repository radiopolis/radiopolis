payRollApp.controller('loginCtrl', ['$scope', 'httpAuth', 'Login', 'requestQueue', '$window', function ($scope, httpAuth, Login, requestQueue, $window) {

	$scope.credentials = {
	    username: null,
	    password: null
	};

	$scope.Login = function (e) {
		$scope.removeErrorCredentials();
		
		if (!$scope.credentials.username || !$scope.credentials.password) {
			$scope.showErrorCredentials();
		}
		else{
			$scope.removeErrorCredentials();

			httpAuth.basic($scope.credentials);
			requestQueue.flush();

			Login.Authenticate();
			
		}
	};

	$scope.showErrorCredentials = function() {
      	$('#output').addClass('alert alert-danger animated fadeInUp').html('Usuario y/o Contraseña inválidos ');

		$scope.credentials = {
			username : null,
			password : null
		};
    };

    $scope.removeErrorCredentials = function() {
      $('#output').removeClass( 'alert alert-danger animated fadeInUp' ).addClass( '' ).html('');
    };

}])