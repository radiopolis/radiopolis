payRollApp.controller('reportLiquidateHours_Ctrl', ['$scope', 'HorasExtra', 'Export', function ($scope, HorasExtra, Export) {

	$scope.filter			= {
		initialDate : new Date(),
		finalDate   : new Date()
	};

	$scope.status = {
		InitDateOpen : false,
		FinalDateOpen : false
	};

	$scope.format = 'dd-MM-yyyy';

	$scope.OpenInitDate = function ($event) {
		$scope.status.InitDateOpen = true;
	};

	$scope.OpenFinalDate = function ($event) {
		$scope.status.FinalDateOpen = true;
	};

	$scope.LiquidateHours 	= function () {
		updateHours($scope.filter);

		$scope.filter.initialDate = moment($scope.filter.initialDate).format('DD/MM/YYYY HH:mm:ss');
		$scope.filter.finalDate = moment($scope.filter.finalDate).format('DD/MM/YYYY HH:mm:ss');
		
		Export.downloadIt($scope.filter, "/ExtraHours/ExtraHours/Detaill/GetFile", 'DetaillLiquidatedHours');
         //HorasExtra.Export.GetFile({}, $scope.filter);
    };

    var updateHours = function (filter) {
    	filter.initialDate.setHours(0);
		filter.initialDate.setMinutes(0);
		filter.initialDate.setSeconds(0);

		filter.finalDate.setHours(23);
		filter.finalDate.setMinutes(59);
		filter.finalDate.setSeconds(59);

		return filter;
    	
    };

}]);