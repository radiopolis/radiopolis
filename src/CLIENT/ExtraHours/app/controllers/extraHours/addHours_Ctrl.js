payRollApp.controller('addHours_Ctrl', ['$scope', 'HorasExtra', 'Horarios', function ($scope, HorasExtra, Horarios) {

    $scope.upstertDetaill = {
        observaciones: '',
        fechas: null
    };
    $scope.extraHourDetaills = [];
	$scope.horaInicial = new Date();
	$scope.horaFinal = new Date();
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.ismeridian = true;

    $scope.horarios     = Horarios.SchedulesByEmployee.GetSchedules();

    $.fn.datepicker.dates['es'] = {
        days: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
        daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
        daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        today: 'Hoy'
    };

    $('#hoursDates').datepicker({
        format: 'mm/dd/yyyy',
        orientation: 'bottom auto',
        language: 'es',
        multidate: true
    });

    $scope.GetExtraHours = function () {
        $scope.extraHours = HorasExtra.Get.GetHorasExtra();
    };

    $scope.GetExtraHoursDetaill = function (_reportId) {
        // TODO: Bloquear otros meses
        $scope.extraHourDetaills = [];
        $scope.extraHourDetaills = HorasExtra.GetDetaill.GetHorasExtraDetalle({ IDReporteHE: _reportId });
        $scope.upstertDetaill.iDReporteHE = _reportId;
    };

    $scope.GetExtraHours();

    $scope.AddDetail = function () {
		FixHours();
        if ($scope.upstertDetaill.IDHoraExtraDetalle == null) {
            $scope.upstertDetaill.estado = null;
            $scope.upstertDetaill.fechas = $scope.upstertDetaill.fechas.split(',');
			$scope.upstertDetaill.horaInicial = $scope.horaInicial;
			$scope.upstertDetaill.horaFinal = $scope.horaFinal;
            
            HorasExtra.AddDetaill.AddExtraHourDetaill({}, $scope.upstertDetaill).$promise.then(function () {
                $scope.GetExtraHoursDetaill($scope.upstertDetaill.iDReporteHE);
                $scope.GetExtraHours();
                $scope.upstertDetaill = {
                    iDReporteHE : $scope.upstertDetaill.iDReporteHE
                };
                setTimeout(function(){
                    ClearDatepicker();
                }, 1000);
                
                $scope.horaInicial  = new Date();
                $scope.horaFinal    = new Date();
            });
        }
    };
	
	var FixHours = function(){
		$scope.horaInicial.setHours( $scope.horaInicial.getHours() - 5 );
		$scope.horaFinal.setHours( 	$scope.horaFinal.getHours()  - 5 );
	};
    
    var ClearDatepicker = function(){
        $('#hoursDates').data('datepicker').setDate(null);
    };

    $scope.DeleteDetaill = function (detaill) {
        HorasExtra.RemoveDetaill.Delete({ iDHoraExtraDetalle : detaill.iDHoraExtraDetalle }).$promise.then(function () {
            $scope.GetExtraHoursDetaill( detaill.iDReporteHE );
        });
    };

    $('#addHoursDetail').on('hide.bs.modal', function() {
         $scope.GetExtraHours();
    })

}])