payRollApp.controller('detaillHours_Ctrl', ['$scope', '$stateParams', 'HorasExtra', function ($scope, $stateParams, HorasExtra) {

    $scope.extraHourDetaills = [];

    var switchOptions = {
        onText: "Aprobada",
        onColor: 'success',
        offColor: 'danger',
        offText: "Rechazada",
        animate: true,
    };

    $('#approveHoursDetail').on('shown.bs.modal', function () {

        $("[name='my-checkbox']").bootstrapSwitch(switchOptions);

        $('#selectAll').on('switchChange.bootstrapSwitch', function (event, state) {
                
            if($scope.extraHourDetaills && $scope.extraHourDetaills.length > 0){
                for(var i = 0 ; i < $scope.extraHourDetaills.length ; i++ ){
                    $('#detaill_'+$scope.extraHourDetaills[i].iDHoraExtraDetalle).bootstrapSwitch('state', state);
                }
            }
        });

        // Actualizamos el valor del $scope, cuando se cambie el estado del switch
        $('.switchDetaill').on('switchChange.bootstrapSwitch', function (event, state) {
            var id = event.currentTarget.id.split('_');
            if($scope.extraHourDetaills && $scope.extraHourDetaills.length > 0){
                for(var i = 0 ; i < $scope.extraHourDetaills.length ; i++ ){
                    if ($scope.extraHourDetaills[i].iDHoraExtraDetalle == parseInt(id[1]) ) {
                        $scope.extraHourDetaills[i].estado = (state)? "A" : "S";
                    };
                }
            }
        });

        setSwitchValue();
    })

    $scope.GetExtraHours = function () {
        $scope.extraHoursApprover = HorasExtra.GetFiltered.GetHorasExtra({ employeeId: $stateParams.employeeId, filter: $stateParams.filter });
    };

    $scope.GetExtraHoursDetaill = function (_reportId) {
        HorasExtra.GetDetaillForEmployee.GetHorasExtraDetalle({ IDReporteHE: _reportId, IDEmpleado : $stateParams.employeeId }).$promise.then(function (_hoursDetaill) {
            $scope.extraHourDetaills = _hoursDetaill;
        });
            
    };

    // recorremos el current detaill, y seg�n el estado le seteamos el switch
    var setSwitchValue = function () {
        if($scope.extraHourDetaills && $scope.extraHourDetaills.length > 0){
            for(var i = 0 ; i < $scope.extraHourDetaills.length ; i++ ){
                var currentStatus   = $scope.extraHourDetaills[i].estado;
                var switchStatus    = (currentStatus == "S") ? false : true;
                $('#detaill_'+$scope.extraHourDetaills[i].iDHoraExtraDetalle).bootstrapSwitch('state', switchStatus);
            }
        }
    };

    $scope.SaveDetaills =  function () {
        if($scope.extraHourDetaills && $scope.extraHourDetaills.length > 0){
            /*var upsertDetaillArray = [];*/
            for(var i = 0 ; i < $scope.extraHourDetaills.length ; i++ ){
                var detaillUpsert = {
                    iDHoraExtraDetalle: $scope.extraHourDetaills[i].iDHoraExtraDetalle,
                    estado: $scope.extraHourDetaills[i].estado
                };

                console.log('Trying to upsert detaill...', detaillUpsert);
                HorasExtra.Update.UpdateExtraHour({}, detaillUpsert);
            };
        }
    };    

    $scope.GetExtraHours();

}])