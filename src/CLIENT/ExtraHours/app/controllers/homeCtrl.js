payRollApp.controller('homeCtrl', ['$scope', '$location', 'Login', 'toastr', 'httpAuth', function ($scope, $location, Login, toastr, httpAuth) {

	$scope.passwords = {};

	var getUserName =  function () {
		Login.Permissions.GetUserPermissions({}).$promise.then(function (permissions) {
			// example: [1 , 'jdmaldonado']
			if (permissions.length  === 3) {
				$scope.isAdmin 	= (permissions[0] === 2)? true : false; 
				$scope.username = permissions[1];
			}
			else{
				toastr.error('Error al cargar permisos', permissions);
			}
		});
	};

	getUserName();

	$scope.Logout = function () {
		httpAuth.cancel();
	};

	$scope.ValidatePassword =  function () {

		if ($scope.passwords.firstPassword === $scope.passwords.confirmPassword) {
			var newPassword = {
				contrasena : $scope.passwords.firstPassword
			};

			Login.Password.ChangePass({}, newPassword).$promise.then(function () {
				$scope.passwords = {};
				$('#changePassModal').modal('hide');
				setTimeout(function () {
					$scope.Logout();
					getUserName();
				}, 3000);
				
			});
		}
		else{
			toastr.error('Las contraseñas no coinciden');
		}
	}
}])