payRollApp.controller('forgotPassword_Ctrl', ['$scope', 'httpAuth', 'Login', 'requestQueue', '$window', 'Notifications', function ($scope, httpAuth, Login, requestQueue, $window, Notifications) {


	$scope.credentials = {
		username : null
	};

	$scope.Forgot = function (e) {
		$scope.removeErrorCredentials();
		
		if (!$scope.credentials.username ) {
			$scope.showErrorCredentials();
		}
		else{
			$scope.removeErrorCredentials();

			Login.ForgotPassword.ChangePass({userName : $scope.credentials.username}).$promise.then(
				function (response) {
					Notifications.customMessage.success('Correo Enviado con éxito');
				},
				function(err){
					Notifications.customMessage.error(err.message);
					console.log(err);
				}
			);
			
		}
	};

	$scope.showErrorCredentials = function() {
      	$('#output').addClass('alert alert-danger animated fadeInUp').html('Usuario requerido !');

		$scope.credentials = {
			username : null
		};
    };

    $scope.removeErrorCredentials = function() {
      $('#output').removeClass( 'alert alert-danger animated fadeInUp' ).addClass( '' ).html('');
    };	

}])