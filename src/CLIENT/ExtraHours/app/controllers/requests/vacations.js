payRollApp.controller('vacations_Ctrl', ['$scope', '$timeout', 'toastr', 'Requests', 'Login', 'Notifications', 'Export', function ($scope, $timeout, toastr, Requests, Login, Notifications, Export) {

    $scope.disableForm = false;

    function getAvalaibleDays() {
        Requests.AvalaibleDays.GetAvalaibleDays().$promise.then(function (data) {
            $scope.periods = data;
            $scope.totalAvailablesDays = getTotalAvalaibleDays(data);
        });
    }

    getAvalaibleDays();

    Login.Permissions.GetUserPermissions({}).$promise.then(function (permissions) {
        if (permissions.length  === 3) {
            $scope.isAdmin 	= (permissions[0] === 2)? true : false; 
            $scope.username = permissions[1];
            $scope.area = permissions[2];
            $scope.getData();
        }
        else{
            toastr.error('Error al cargar permisos', permissions);
        }
    });
    
    $scope.vacationRequest = {};
    $scope.areValidDaysCant = true;

    $scope.downloadFile = function() {
        Export.downloadIt({}, "/ExtraHours/Request/GetFile/"+$scope.requestId, 'FormatoVacaciones', true);
    }

    $scope.title = 'Vacaciones';
    $scope.getData = function(){
        // type: 1 --> Vacations
        $scope.data = {
            type : 'Vacations',
            // Mias
            myRequests: Requests.MyRequests.GetMyRequests({ type: 1 }),
            // Solicitudes de Area (13  es porque es vacaciones)
            areaRequests: Requests.GetRequests.GetRequestsByFilter({ area: 13, type: 1, status: 'Aprobado,Autorizado,Liquidado' }),
            // Por Aprobar
            approveRequests: Requests.ApproveRequests.GetApproveRequests({ type: 1, status: 'Creado' }) 
        };
        if($scope.area === 13) {
            $scope.data.showAuthorized = true;
            // Por Autorizar 
            $scope.data.authorizatedRequests = Requests.GetRequests.GetRequestsByFilter({ area: 13, type: 1, status: 'Aprobado,Documentacion' });
        }

        if($scope.area === 10) {
            $scope.data.showliquidated = true;
            // Por Liquidar
            $scope.data.liquidatedRequests = Requests.GetRequests.GetRequestsByFilter({ area: 10, type: 1, status: 'Autorizado' });
        };
    };

    $scope.changeVacationsDays =  function(){
        var disfrutados = ($scope.vacationRequest.disfrutados === undefined)? 0 : parseInt($scope.vacationRequest.disfrutados);
        var pagos = ($scope.vacationRequest.pagos === undefined)? 0 : parseInt($scope.vacationRequest.pagos);

        if( (disfrutados + pagos) > $scope.totalAvailablesDays  ){
            $scope.areValidDaysCant = false;
        }else{
            $scope.areValidDaysCant = true;
        }
    };

    $scope.changeRequestStatus = function(requestData){
        $scope.requestId =  requestData.id;
        var possibleStatus = getPosibleNewStatus(requestData.status);

        $scope.showDownloadFile = (requestData.status === 'Documentacion')? true : false;
            
        
        if(possibleStatus){
            $scope.posibleStatus = possibleStatus;

            $('#changeStatusModal').modal('show');
        }
    };

    $scope.updateRequestStatus = function(){
        var list = [
            {
                requestId : $scope.requestId,
                newStatus : $scope.newStatus.status,
                note : $scope.note
            }
        ];

        $('#changeStatusModal').modal('hide');

        Requests.ChangeRequestStatus.Change({}, list).$promise.then(
            function (response) {
                Notifications.customMessage.success('La solicitud ha sido actualizada satifactoriamente.');
                getAvalaibleDays();
            },
            function(err){
                Notifications.customMessage.error('Ha ocurrido un error al intentar actualizar la solicitud... ');
                console.error(err);
                getAvalaibleDays();
            }
        );
    };

    $scope.requestVacations = function(){
        $scope.vacationRequest.type = 1;

        if($scope.requestId){
            Requests.UpdateRequest.Update({ requestId: $scope.requestId },$scope.vacationRequest)
            .$promise.then(function () {
                $('#vacationsModal').modal('hide');
            });
        }
        else{
            Requests.CreateRequest.Create($scope.vacationRequest)
            .$promise.then(function () {
                $('#vacationsModal').modal('hide');
            });
        }
    };

    $scope.openHistoricModal = function(requestData) {
        Requests.GetHistoricById.Get({id: requestData.id})
        .$promise.then(
            function (data) {
                $scope.transactions = data;
                console.log($scope.transactions);
            },
            function(err){
                console.error(err);
            }
        );
        $('#historicModal').modal('show');
    }

    $scope.openModal = function(requestData, disableForm){
        $scope.disableForm = disableForm;
        $scope.requestId =  requestData.id;
        var extendedData = JSON.parse(requestData.extendedData);

        $('#vacationsModal').modal('show');

        $scope.vacationRequest.disfrutados = extendedData.disfrutados;
        $scope.vacationRequest.pagos = extendedData.pagos;
        $scope.vacationRequest.observaciones = extendedData.observaciones;

        // First update the $scope, then the datepicker data
        $scope.vacationRequest.dates = extendedData.dates;
        $('#vacationDates').data('datepicker').setDate(extendedData.dates.split(','));

        if(!$scope.$$phase) {
            $scope.$apply();
        };
    };

    // TODO: Evaluate don't remove request oif status !== creado
    $scope.removeRequest = function(requestData){
        swal({
            title: 'Est&aacute; seguro que desea eliminar esta solicitud ?',
            text: 'La solicitud ' + requestData.description + ' va a ser eliminada',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
        function(){
            Requests.DeleteRequest.Delete({requestId: requestData.id}).$promise.then(
				function (response) {
					swal('Eliminado!', 'La solicitud ha sido eliminada satifactoriamente.', 'success');
                    $scope.getData();
				},
				function(err){
					swal('Error!', 'Ha ocurrido un error al intentar eliminar la solicitud...'+err, 'error');
				}
			);
            
        });
    };

    $('#vacationsModal').on('hidden.bs.modal', function (e) {
        $scope.disableForm = false;
        clearForm();
        $scope.getData();
    });

    $('#changeStatusModal').on('hidden.bs.modal', function (e) {
        clearForm();
        $scope.getData();
    });

    $('#vacationDates').datepicker({
        format: 'dd/mm/yyyy',
        orientation: 'bottom auto',
        language: 'es',
        multidate: true,
        clearBtn : true,
        daysOfWeekDisabled: [0,6],
        datesDisabled : $scope.holidays
    });

    $('#vacationDates').datepicker().on('changeDate', function(e, a) {

        if(e.dates.length > $scope.vacationRequest.disfrutados){
            toastr.error('S&oacute;lo puede seleccionar ' + $scope.vacationRequest.disfrutados + ' d&iacute;as');
            e.dates.pop();
            $(this).data('datepicker').setDate(e.dates);
        }
    });

    Requests.Holidays.GetHolidays().$promise.then(function (holidays) {
        $('#vacationDates').datepicker('setDatesDisabled', holidays);
    });

    function clearForm(){
        $scope.vacationRequest = {};
        $scope.areValidDaysCant = true;
        $scope.note = null;
        $scope.newStatus = null;
        clearDatepicker();

        if(!$scope.$$phase) {
            $scope.$apply();
        };
    };

    function clearDatepicker(){
        $('#vacationDates').data('datepicker').setDate(null);
    };

    function getTotalAvalaibleDays(periods){
        var cant = 0;
        if(!periods){
            return cant;
        }

        for(var i = 0 ; i < periods.length ; i ++){
            cant += periods[i].diasCalculate;
        };

        return cant;
    };

    function getPosibleNewStatus(currentStatus){
        switch (currentStatus) {
            case 'Creado':
                return [
                    {
                        label: 'Aprobar',
                        status: 'Aprobado'
                    },
                    {
                        label: 'Rechazar',
                        status: 'Rechazado'
                    }
                ]
            break;
            case 'Aprobado':
                return [
                    {
                        label: 'Rechazar',
                        status: 'Rechazado'
                    },
                    {
                        label: 'Pendiente de Documentaci&oacute;n',
                        status: 'Documentacion'
                    }
                ]
            break;
            case 'Autorizado':
                return [
                    {
                        label: 'Liquidar',
                        status: 'Liquidado'
                    },
                    {
                        label: 'Rechazar',
                        status: 'Rechazado'
                    }
                ]
            break;
            case 'Documentacion':
                return [
                    {
                        label: 'Autorizar',
                        status: 'Autorizado'
                    },
                    {
                        label: 'Rechazar',
                        status: 'Rechazado'
                    }
                ]
            break;
        
            default:
                return null;
            break;
        }
    };

    

}])
