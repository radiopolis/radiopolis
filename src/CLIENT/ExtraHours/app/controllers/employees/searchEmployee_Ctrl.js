﻿payRollApp.controller('searchEmployee_Ctrl', ['$scope', 'Areas', 'Employees', 'Export', function ($scope, Areas, Employees, Export) {

    $scope.filterOptions = {
        filterText: ''
    };

    // Como Angular es asíncrono, hay que esperar a que traiga las areas 
    // y las empresas para hacer el mapeo del empleado
    Areas.Area.GetAreas().$promise.then(function (_areas) {
        $scope.areas = _areas;

        Employees.company.GetCompanies().$promise.then(function (_companies) {
            $scope.factories = _companies;

            Employees.offices.GetOffices().$promise.then(function (_offices) {
                $scope.offices = _offices;

                Employees.employee.GetEmployees({}).$promise.then(function (data) {

                    $scope.employees = UpdateData(data);
                });
            });
        });
    });

    var UpdateData = function (employees) {
        if (employees.length > 0) {
            for (var i = 0; i < employees.length; i++) {

                $scope.areas.map(function (objeto) {
                    if (angular.equals(objeto.iDArea, employees[i].iDArea)) {
                        employees[i].area = objeto.nombre;
                    }
                });

                $scope.factories.map(function (objeto) {
                    if (angular.equals(objeto.iDEmpresa, employees[i].iDEmpresa)) {
                        employees[i].empresa = objeto.nombre;
                    }
                });

                $scope.offices.map(function (objeto) {
                    if (angular.equals(objeto.iDCargo, employees[i].iDCargo)) {
                        employees[i].cargo = objeto.cargo;
                    }
                });

            }

            return employees;
        }
    }

    $scope.gridOptions = {
        data: 'employees',
        showGroupPanel: true,
        enableColumnResize: true,
        filterOptions: $scope.filterOptions,
        columnDefs: [
                    { field: 'cedula', displayName: 'Cedula'},
                    { field: 'nombres', displayName: 'Nombres' },
                    { field: 'apellidos', displayName: 'Apellidos' },
                    { field: 'fechaIngreso', displayName: 'Fecha Ingreso', cellFilter: 'date:\'dd-MM-yyyy\'' },
                    { field: 'cargo', displayName: 'Cargo' },
                    { field: 'telefono', displayName: 'Teléfono' },
                    { field: 'movil', displayName: 'Celular' },
                    { field: 'email', displayName: 'Correo' },
                    { field: 'area', displayName: 'Area' },
                    { field: 'empresa', displayName: 'Empresa' },
                    ]
    };

    $scope.EmployeeData = function () {

    Export.downloadIt("", "/ExtraHours/Employees/GetFile", 'EmployeeData');

    };

}])