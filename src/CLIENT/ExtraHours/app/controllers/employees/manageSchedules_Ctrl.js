﻿payRollApp.controller('manageSchedules_Ctrl', ['$scope', 'Horarios', 'Employees', 'Notifications', function ($scope, Horarios, Employees, Notifications) {

    $scope.showTable = false;
    $scope.editMode = false;
    $scope.createMode = false;
    $scope.deleteMode = false;
    $scope.customCreateMode = false;
    $scope.invalidSchedule = false;
    $scope.validSchedule = false;
    $scope.newSchedule = {
        description: ""
    };    
    $scope.daysCheckBox = {
        lunes: false,
        martes: false,
        miercoles: false,
        jueves: false,
        viernes: false,
        sabado: false,
        domingo: false
    };
    //TimePicker
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.ismeridian = true;
    $scope.scheduleToDelete = {};
    $scope.scheduleToEdit = {};

    // Del select a la Tabla
    $scope.CargarHorario = function () {

        $scope.LimpiarHorario();
        var currentIdHorario = $scope.scheduleToEdit.currentIdHorario;

        $scope.horarios.map(function (objeto) {
            if (angular.equals(objeto.iDHorario, currentIdHorario)) {
                $scope.MostrarHorario();

                for(var property in objeto) {
                    if(objeto.hasOwnProperty(property)) {
                        objeto[property] = getTimeSpanFromString(objeto[property], (property === 'descripcion'));
                    }
                };
                $scope.currentSchedule = objeto;
            };
        });
    }

    $scope.EliminarHorario = function () {
        var currentIdHorario = $scope.scheduleToDelete.currentIdHorario;
        Horarios.Delete.DeleteSchedule({ ScheduleId: currentIdHorario }).$promise.then(function () {
            clear();
        });
    };

    $scope.EvaluateValidHours = function () {

        if ($scope.inHour != null && $scope.inHour != undefined &&
            $scope.outHour != null && $scope.outHour != undefined) {

            return false;
        }
        return true;
    };

    $scope.EvaluateValidScheduler = function () {

        for (var property in $scope.currentSchedule) {
            if ($scope.currentSchedule[property] && $scope.currentSchedule[property].hours != null) {
                return false;
            };
        }
        return true;
    };

    $scope.GetHorarios = function () {
        $scope.horarios = Horarios.Get.GetHorarios();
    };

    $scope.GuardarHorario = function () {

        for(var property in $scope.currentSchedule) {
            if($scope.currentSchedule.hasOwnProperty(property)) {
                $scope.currentSchedule[property] = getStringFromTimeSpan($scope.currentSchedule[property]);
            }
        }

        $scope.currentSchedule.descripcion = $scope.newSchedule.description;
        Horarios.Create.AddHorario({}, $scope.currentSchedule);
    };

    $scope.GuardarHorarioRapido = function () {

        for (var property in $scope.daysCheckBox) {
            if ($scope.daysCheckBox[property]) {

                $scope.currentSchedule[property + 'Entrada'].hours  = $scope.inHour.getHours();
                $scope.currentSchedule[property + 'Entrada'].minutes = $scope.inHour.getMinutes();
                $scope.currentSchedule[property + 'Salida'].hours   = $scope.outHour.getHours();
                $scope.currentSchedule[property + 'Salida'].minutes = $scope.outHour.getMinutes();
            };
        }
    };

    $scope.GuardarHorasPorDia = function () {
        if ($scope.currentSchedule[$scope.modalDay + 'Entrada'] === null || $scope.currentSchedule[$scope.modalDay + 'Salida'] === null) {

            $scope.currentSchedule[$scope.modalDay + 'Entrada'] = {
                hours : $scope.inHour.getHours(),
                minutes : $scope.inHour.getHours()
            };

            $scope.currentSchedule[$scope.modalDay + 'Salida'] = {
                hours : $scope.outHour.getHours(),
                minutes : $scope.outHour.getHours()
            };
        }
        else{
            $scope.currentSchedule[$scope.modalDay + 'Entrada'].hours = $scope.inHour.getHours();
            $scope.currentSchedule[$scope.modalDay + 'Entrada'].minutes = $scope.inHour.getMinutes();
            $scope.currentSchedule[$scope.modalDay + 'Salida'].hours = $scope.outHour.getHours();
            $scope.currentSchedule[$scope.modalDay + 'Salida'].minutes = $scope.outHour.getMinutes();
        };
    };

    $scope.LimpiarHorario = function () {

        $scope.currentSchedule = {
            lunesEntrada: {},
            martesEntrada: {},
            miercolesEntrada: {},
            juevesEntrada: {},
            viernesEntrada: {},
            sabadoEntrada: {},
            domingoEntrada: {},
            lunesSalida: {},
            martesSalida: {},
            miercolesSalida: {},
            juevesSalida: {},
            viernesSalida: {},
            sabadoSalida: {},
            domingoSalida: {}
        };
    };

    $scope.ModificarHoras = function (dia) {
        //console.log("dia: "+dia);
        $scope.modalDay = dia;

        if ($scope.currentSchedule[dia + 'Entrada'] === null) {
            $scope.inHour = new Date();
            $scope.outHour = new Date();
        }
        else {
            $scope.inHour = new Date(2015, 0, 1, ($scope.currentSchedule[dia + 'Entrada'].hours), $scope.currentSchedule[dia + 'Entrada'].minutes, 0, 0);
            $scope.outHour = new Date(2015, 0, 1, ($scope.currentSchedule[dia + 'Salida'].hours), $scope.currentSchedule[dia + 'Salida'].minutes, 0, 0);
        }
    };

    $scope.MostrarHorario = function (personalizado) {
        if (personalizado) {
            $scope.customCreateMode = true;            
        }
        else {
            $scope.customCreateMode = false;
            $scope.inHour = null;
            $scope.outHour = null;
        }
        $scope.showTable = true;
        $scope.LimpiarHorario();
    };

    $scope.OcultarHorario = function () {
        $scope.showTable = false;
    };

    $scope.UpdateHorario = function () {
        for(var property in $scope.currentSchedule) {
            if($scope.currentSchedule.hasOwnProperty(property)) {
                $scope.currentSchedule[property] = getStringFromTimeSpan($scope.currentSchedule[property]);
            }
        }

        console.log($scope.currentSchedule);
        Horarios.Update.UpdateHorario({}, $scope.currentSchedule);
    };

    $scope.ValidarEliminarHorario = function (selectedIdHorario) {

        $scope.invalidSchedule = false;
        $scope.validSchedule = false;

        Employees.employeesByScheduleID.GetEmployees({ ScheduleId: selectedIdHorario }).$promise.then(function (_employees) {
            if (_employees.length > 0) {
                $scope.asociatedEmployees = _employees;
                $scope.invalidSchedule = true;
            }
            else {
                $scope.horarios.map(function (objeto) {
                    if (angular.equals(objeto.iDHorario, selectedIdHorario)) {
                        $scope.deleteSchedule = objeto.descripcion;
                    }
                })
                $scope.validSchedule = true;
            }
        });
    };

    $scope.openFastScheduleModal = function () {
        $('#FastSchedule').modal('show');
        $('[data-toggle=popover]').popover('hide');
    };

    var getTimeSpanFromString = function (stringDate, isDescription) {

        if (isDescription) {
            return stringDate;
        };

        if (stringDate && typeof stringDate  === 'string') {
            var dateSplited = stringDate.split(':');
            if (dateSplited.length === 3) {
                return {
                    hours   : parseInt(dateSplited[0]),
                    minutes : parseInt(dateSplited[1]),
                    seconds : parseInt(dateSplited[2])
                }
            }
            else{
                return stringDate;
            }
        }
        else{
            if (typeof stringDate  === 'number') {
                return stringDate;
            }
            else{
                return null;    
            }
            
        }
    };

    var getStringFromTimeSpan = function (timeSpanDate) {
        if (timeSpanDate && timeSpanDate.hours) {
            return [fillwithZero(timeSpanDate.hours, 2), fillwithZero(timeSpanDate.minutes, 2), fillwithZero(0, 2) ].join(':');
        }
        else{
            if (timeSpanDate === null) {
                return null;
            }else if (typeof timeSpanDate == 'number') {
                return timeSpanDate;
            }
            else if (typeof timeSpanDate === 'object' && Object.keys(timeSpanDate).length === 0) {
                return null;
            }
            else{
                return timeSpanDate;    
            }
            
        }
    }

    var fillwithZero = function (number, cant) {
        var zero = '0';
        var pad = new Array(1 + cant).join(zero);
        return (pad + number).slice(-pad.length);
    };

    var clear = function () {
        $scope.showTable = false;
        $scope.editMode = false;
        $scope.createMode = false;
        $scope.deleteMode = false;
        $scope.customCreateMode = false;
        $scope.invalidSchedule = false;
        $scope.validSchedule = false;
        $scope.newSchedule = {
            description: ""
        };    
        $scope.daysCheckBox = {
            lunes: false,
            martes: false,
            miercoles: false,
            jueves: false,
            viernes: false,
            sabado: false,
            domingo: false
        };
    };

    $scope.LimpiarHorario();
}])