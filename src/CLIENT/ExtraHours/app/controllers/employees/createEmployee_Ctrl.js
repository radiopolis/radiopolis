payRollApp.controller('createEmployee_Ctrl', ['$scope', 'Areas', 'Employees', 'Horarios', 'Notifications', 'CentersCost', function ($scope, Areas, Employees, Horarios, Notifications, CentersCost) {

    $scope.employee = {};

    $scope.Limpiar = function () {
        $scope.employee = {};
    };

    $scope.roles = [
        { iDRol : 1, nombre : 'Empleado'},
        { iDRol : 2, nombre : 'Administrador'}
    ];

    $scope.Save = function () {
        if ($scope.employee.iDEmpleado !== undefined) {
            console.log('Update', $scope.employee);
            Employees.update.UpdateEmployee({}, $scope.employee);
        }
        else {
            console.log('Add', $scope.employee);
            Employees.add.AddEmployee({}, $scope.employee);
        }
        $scope.Limpiar();
    };

    $scope.Validate = function (_identification) {
        Employees.validate.ValidateEmployee({ identification: _identification }).$promise.then(function (_employee) {
            if (_employee.iDEmpleado !== undefined) {
                // la cedula llega como string pero el input sólo acepta numbers
                _employee.cedula = parseInt(_employee.cedula);

                _employee.fechaIngreso = moment(_employee.fechaIngreso).format('YYYY/MM/DD');
                _employee.fechaTerminacionContrato = moment(_employee.fechaTerminacionContrato).format('YYYY/MM/DD');

                $('#fechaIngreso').datepicker('setDate', _employee.fechaIngreso);
                $('#fechaTerminacion').datepicker('setDate', _employee.fechaTerminacionContrato);

                console.log(_employee);
                $scope.employee = _employee;
            }
            else {
                Notifications.info.EmployeeDoesNotExist();
                $scope.employee = {};
                $scope.employee.cedula = _identification;
            }
        });
    };
    
    $scope.areas        = Areas.Area.GetAreas();
    $scope.subAreas     = Areas.subAreas.GetSubAreas();
    $scope.factories    = Employees.company.GetCompanies();
    $scope.horarios     = Horarios.Get.GetHorarios();
    $scope.empleados    = Employees.employee.GetEmployees();
    $scope.centerCosts  = CentersCost.centerCost.GetCentersCost();
    $scope.offices      = Employees.offices.GetOffices();
    $scope.contracts    = Employees.contracts.GetContracts();


    // Evaluamos cuando hay un cambio en centro de Costo para traer los sub centros de costo
    $scope.$watch('employee.iDCentro', function(newValue, oldValue) {
        if (newValue != undefined) {
            $scope.subCenterCosts = CentersCost.subCenterCost.GetSubCentersCost({Id : newValue});
        };
    }, true);

    $('#Employee_Schedule').change(function () {
        if($('#Employee_Schedule option:selected').length > 2) {
            swal('Cantidad Máxima de Horarios Excedida!', 'Sólo puede seleccionar 2 horarios para un mismo empleado', 'warning')
            $scope.employee.iDHorario = $scope.employee.iDHorario.slice(0, 2);
            $scope.$apply();
        }
    });

    $('#fechaIngreso').datepicker({
        format: 'yyyy/mm/dd',
        orientation: 'bottom auto',
        language: 'es',
        locale:'es'
    });

    $('#fechaTerminacion').datepicker({
        format: 'yyyy/mm/dd',
        orientation: 'bottom auto',
        language: 'es',
        locale:'es'
    });

    /* DATEPICKER  (Fecha Ingreso)*/
    $scope.hoy = function () {
        $scope.employee.fechaIngreso = new Date();
    };

    $scope.limpiarFecha = function () {
        $scope.employee.fechaIngreso = null;
    };

    $scope.limpiarFecha();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.open2 = function ($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened2 = true;
    };

    $scope.format = 'YYYY/MM/DD';
    /**/

}])