payRollApp.directive('loginForm', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {


            scope.$on('event:auth-loginRequired', function () {
                $window.location.href = '#/';
            });
            
            scope.$on('event:auth-loginConfirmed', function () {
                $window.location.href = '#/Home';
            });
        }
    }
});