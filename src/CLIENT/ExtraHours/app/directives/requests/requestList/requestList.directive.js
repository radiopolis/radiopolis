angular.module('payRollApp')
    .directive('requestList', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            data: '=',
            openModal: '&',
            removeRequest :'&',
            changeRequestStatus : '&',
            openHistoricModal : '&'
        },
        controller: 'requestList_Ctrl',
        templateUrl: 'app/directives/requests/requestList/requestList.html'
    };
})
