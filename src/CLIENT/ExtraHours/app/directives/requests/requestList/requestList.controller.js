angular.module('payRollApp')
    .controller('requestList_Ctrl', ['$scope', function ($scope) {

        var buttonsGridHtml =
        '<div class="text-center buttons-grid-container">'+
            '<button ng-if="row.entity.status === \'Creado\' || row.entity.status === \'Rechazado\'" class="btn btn-sm btn-primary button-grid" ng-click="editRequest(row, false)">'+ 
                '<span class="glyphicon glyphicon-pencil"></span>'+
            '</button>'+
            '<button ng-if="row.entity.status === \'Creado\' || row.entity.status === \'Rechazado\'" class="btn btn-sm btn-danger button-grid" ng-click="deleteRequest(row)">'+
                '<span class="glyphicon glyphicon-trash"></span>'+
            '</button>'+
            '<button class="btn btn-sm btn-info button-grid" ng-click="showHistoric(row)">'+
                '<span class="glyphicon glyphicon-list-alt"></span>'+
            '</button>'+
        '</div>';

        var buttonChangeStatusHtml = 
        '<div class="text-center buttons-grid-container">'+
            '<button class="btn btn-sm btn-default button-grid" ng-click="editRequest(row, true)">'+ 
                '<span class="glyphicon glyphicon-zoom-in"></span>'+
            '</button>'+
            '<button class="btn btn-sm btn-primary button-grid" ng-click="changeRequestStatus({requestData : row.entity})">'+ 
                '<span class="glyphicon glyphicon-share"></span>'+
            '</button>'+
            '<button class="btn btn-sm btn-info button-grid" ng-click="showHistoric(row)">'+
                '<span class="glyphicon glyphicon-list-alt"></span>'+
            '</button>'+
        '</div>';

        function getColumns(dataName) {
            var columns = [
                { field: 'description', displayName: 'Descripción' },
                { field: 'note', displayName: 'Notas' },
                { field: 'userRequest.name', displayName: 'Creado por' },
                { field: 'dateRequest', displayName: 'Fecha Requerimiento', cellFilter: 'date:\'dd-MM-yyyy\'' },
                { field: 'responsableRequest.name', displayName: 'Responsable' },
                {
                    field: 'status',
                    displayName: 'Estado',
                    cellTemplate: '<div class="cellStatus"><span class="label label-{{COL_FIELD | statusRequest }}">{{COL_FIELD}}</span></div>'
                }
            ]
            if(dataName === 'data.myRequests'){
                columns.unshift(
                    { cellTemplate: buttonsGridHtml }
                );
            }
            else{
                columns.unshift(
                    { cellTemplate: buttonChangeStatusHtml }
                );
            }

            return columns;
        };

        function getOptions(dataName) {
            return {
                data: dataName,
                showGroupPanel: true,
                enableColumnResize: true,
                columnDefs: getColumns(dataName)
            };
        }

        $scope.editRequest = function (row, disableForm) {
            $scope.openModal({
                requestData : row.entity,
                disableForm : disableForm
            });
        };

        $scope.showHistoric = function(row) {
            $scope.openHistoricModal({
                requestData : row.entity
            });
        };

        $scope.deleteRequest = function (row) {
            $scope.removeRequest({requestData : row.entity});
        };

        $scope.myRequestsOptions = getOptions('data.myRequests');
        $scope.areaRequestsOptions = getOptions('data.areaRequests');
        $scope.approveRequestsOptions = getOptions('data.approveRequests');

        if($scope.data && $scope.data.showAuthorized){
            $scope.authorizedRequestsOptions = getOptions('data.authorizatedRequests');
        }

        if($scope.data && $scope.data.showliquidated) {
            $scope.liquidatedRequestsOptions = getOptions('data.liquidatedRequests');
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $(window).resize();
        });

    }])
