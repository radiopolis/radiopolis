payRollApp.filter('status_filter', function () {

    return function (input, color, disable) {

        if (input) {
            if (disable) {
                return ( input == "A" || input == "L" )? "disabled" : "";
            }
            else{
                if (color) {
                    switch (input){
                        case "S" : return 'danger';
                        break;
                        case "A" : return 'info';
                        break;
                        case "L" : return 'success';
                        break;
                        default: return 'danger';
                        break;
                    }
                }
                else{
                    switch (input){
                        case "S" : return 'Sin Aprobar';
                        break;
                        case "A" : return 'Aprobadas';
                        break;
                        case "L" : return 'Liquidadas';
                        break;
                        default: return 'Sin Aprobar';
                        break;
                    }
                };
            };
        };
            
    };

});