payRollApp.filter('statusRequest', function () {

    return function (input, disable) {

        if (input) {
            if (disable) {
                return (input == 'Aprobado' || input == 'Liquidado') ? 'disabled' : '';
            }
            else {
                switch (input) {
                    case 'Creado': return 'warning';
                        break;
                    case 'Aprobado': return 'success';
                        break;
                    case 'Rechazado': return 'danger';
                        break;
                    case 'Autorizado': return 'info';
                        break;
                    case 'Liquidado': return 'default';
                        break;
                    default: return 'info';
                        break;
                }
            };
        };

    };

});