payRollApp.filter('monthName_filter', function () {

    return function (input) {

        if ( _.isNumber(input) && input >= 0 && input < 12) {
            var months = [  
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre',
                'Diciembre'
            ];

            return months[input];
        }
        else{
            return input;
        }
    };

});