﻿payRollApp.filter('hoursFormat_filter', function () {

    return function (input) {

        var _amPm = '';

        function HourFormat(hour) {
            var newHour = '';
            if (hour !== undefined) {
                if (hour > 12) {
                    hour -= 12;
                    if (hour < 10) {
                        newHour = '0' + hour.toString();
                    }
                    else {
                        newHour = hour.toString();
                    }
                    _amPm = 'PM';                    
                }
                else {
                    if (hour < 10) {
                        newHour = '0' + hour.toString();
                    }
                    else {
                        newHour = hour.toString();
                    }
                    _amPm = 'AM';
                }
            }
            return newHour;
        }

        function MinutesFormat(minutes) {
            var newMinutes = '';
            if (minutes !== undefined) {
                if (minutes < 10 ) {
                    newMinutes =  '0'+minutes.toString();
                }
                else {
                    newMinutes = minutes.toString();
                }
            }
            return newMinutes;
        }
        
        if (input !== undefined && input !== null) {
            if (Object.keys(input).length === 0) {
                return null
            }
            return HourFormat(input.hours) + ':' + MinutesFormat(input.minutes) + ' ' + _amPm;
        }
        else {
            return null;
        }

    };

});