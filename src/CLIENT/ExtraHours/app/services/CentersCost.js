payRollApp.factory('CentersCost', ['$resource', 'SERVER', function ($resource, SERVER) {

    return {
        centerCost:
            // $resource('app/jsons/centerCost.json', { },
            $resource(SERVER+'/ExtraHours/CostCenter', { },
                {
                    "GetCentersCost": { method: "GET", params: { }, isArray: true }
                }
            ),
        addCenterCost:
            $resource(SERVER+'/ExtraHours/CostCenter/Add', {},
                {
                    "Add": { method : "POST", params: {}, isArray: false }
                }
            ),
        subCenterCost:
            // $resource('app/jsons/subCenterCost.json', { centerCostId: '@centerCostId' },
            $resource(SERVER+'/ExtraHours/SubCostCenter/:Id', { Id: '@Id' },
                {
                    "GetSubCentersCost": { method: "GET", params: { Id: '@Id' }, isArray: true }
                }
            ),
        addSubCenterCost:
            $resource(SERVER+'/ExtraHours/CenterCosts/subCenterCost/Add/:centerCostId', { centerCostId: '@centerCostId' },
                {
                    "Add": { method : "POST", params: { id: '@centerCostId' }, isArray: false }
                }
            )
    };

}]);