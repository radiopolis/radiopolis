﻿payRollApp.factory('Employees', ['$resource', 'SERVER', function ($resource, SERVER) {

    return {
        employee:
            $resource(SERVER+'/ExtraHours/Employees', {},
                {
                    "GetEmployees": { method: "GET", params: {}, isArray: true }
                }
            ),
       	add:
       		$resource(SERVER+'/ExtraHours/Employees/Add', {},
	       		{
	       			"AddEmployee": { method : "POST", params: {}, isArray: false }
	       		}
	       	),
       	update:
       		$resource(SERVER+'/ExtraHours/Employees/Update', {},
	       		{
	       		    "UpdateEmployee": { method: "PUT", params: {}, isArray: false }
	       		}
	       	),
       	validate:
       		$resource(SERVER+'/ExtraHours/Employees/:identification', { identification: '@identification' },
	       		{
	       		    "ValidateEmployee": { method: "GET", params: { identification: '@identification' }, isArray: false }
	       		}
	       	),
	  	company:
       		$resource(SERVER+'/ExtraHours/Employees/Company', {},
	       		{
	       		    "GetCompanies": { method: "GET", params: {}, isArray: true, ignoreLoadingBar: true }
	       		}
	       	),
	  	employeesByScheduleID:
       		$resource(SERVER+'/ExtraHours/Employees/ScheduleId/:ScheduleId', { ScheduleId: '@ScheduleId' },
	       		{
	       		    "GetEmployees": { method: "GET", params: { ScheduleId: '@ScheduleId' }, isArray: true }
	       		}
	       	),
       	offices:
	    	$resource(SERVER+'/ExtraHours/Office', { },
	       		{
	       		    "GetOffices": { method: "GET", params: { }, isArray: true }
	       		}
	       	),
	    contracts:
	    	$resource(SERVER+'/ExtraHours/Contracts', { },
	       		{
	       		    "GetContracts": { method: "GET", params: { }, isArray: true }
	       		}
	       	)
    };

}]);