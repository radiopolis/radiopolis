﻿payRollApp.factory('Requests', ['$resource', '$http', 'SERVER', function ($resource, $http, SERVER) {
    return {
        MyRequests:            
            $resource(SERVER + '/ExtraHours/Request/My/:type', { type: '@type' },
            // $resource('app/jsons/vacations1.json', { },
                {
                    "GetMyRequests": { method: "GET", params: { type: '@type' }, isArray: true }
                }
            ),
        GetRequests:
            $resource(SERVER + '/ExtraHours/Request/Area/:area/:type/:status', { area: '@area', type: '@type', status: '@status'},
            // $resource('app/jsons/vacations2.json', {},
                {
                    "GetRequestsByFilter": { method: "GET", params: { area: '@area', type: '@type', status: '@status' }, isArray: true }
                }
            ),
        ApproveRequests:
            $resource(SERVER + '/ExtraHours/Request/Approve/:type/:status', { type: '@type', status: '@status' },
            // $resource('app/jsons/vacations1.json', {},
                {
                    "GetApproveRequests": { method: "GET", params: { type: '@type', status: '@status' }, isArray: true }
                }
            ),
        CreateRequest:
            $resource(SERVER + '/ExtraHours/Request/Vacations/Add', { },
            // $resource('app/jsons/vacations3.json', {},
                {
                    "Create": { method: "POST", params: { }, isArray: false }
                }
            ),
        UpdateRequest:
            $resource(SERVER + '/ExtraHours/Request/Vacations/Update/:requestId', { requestId: '@requestId' },
            // $resource('app/jsons/vacations3.json', {},
                {
                    "Update": { method: "PUT", params: { requestId: '@requestId' }, isArray: false }
                }
            ),
        DeleteRequest:
            $resource(SERVER + '/ExtraHours/Request/Vacations/Delete/:requestId', { requestId: '@requestId' },
            // $resource('app/jsons/vacations3.json', {},
                {
                    "Delete": { method: "DELETE", params: { requestId: '@requestId' }, isArray: false }
                }
            ),
        Holidays:
            $resource(SERVER + '/ExtraHours/Utils/Holidays', { },
            // $resource('app/jsons/holidays.json', {},
                {
                    "GetHolidays": { method: "GET", params: { }, isArray: true }
                }
            ),
        AvalaibleDays:
            $resource(SERVER + '/ExtraHours/Request/Vacations/AvalaibleDays', { },
            // $resource('app/jsons/avalaibleDays.json', {},
                {
                    "GetAvalaibleDays": { method: "GET", params: { }, isArray: true }
                }
            ),
        ChangeRequestStatus:
            $resource(SERVER + '/ExtraHours/Request/ChangeStatus', { },
            // $resource('app/jsons/avalaibleDays.json', {},
                {
                    "Change": { method: "PUT", params: { }, isArray: false }
                }
            ),
        GetFileName: function(requestId){
            return $http({
                method: 'GET',
                url: SERVER + '/ExtraHours/Request/GetFile/'+ requestId
            })
        },
        GetHistoricById: 
            $resource(SERVER + '/ExtraHours/Request/Trace/:id', { id : '@id' },
                {
                    "Get": { method: "Get", params: { id : '@id' }, isArray: true }
                }
            )
    }
}]);