payRollApp.factory('HorasExtra', ['$resource', 'SERVER', function ($resource, SERVER) {

    return {
        Get:
            $resource(SERVER+'/ExtraHours/ExtraHours', {},
            // $resource(SERVER+'/ExtraHours/app/jsons/hour.json', {},
                {
                    "GetHorasExtra": { method: "GET", params: {}, isArray: true }
                }
            ),
        GetFiltered:
            $resource(SERVER+'/ExtraHours/ExtraHours/DetaillFiltered/:employeeId/:filter', { employeeId: '@employeeId', filter: '@filter' },
                {
                    "GetHorasExtra": { method: "GET", params: { employeeId: '@employeeId', filter: '@filter' }, isArray: true }
                }            
            ),
        GetDetaill:
                $resource(SERVER+'/ExtraHours/ExtraHours/Detaill/:IDReporteHE', { IDReporteHE: '@IDReporteHE' },
                // $resource(SERVER+'/ExtraHours/app/jsons/DETAILHOUR.json', { IDReporteHE: '@IDReporteHE' },                
                {
                    "GetHorasExtraDetalle": { method: "GET", params: { IDReporteHE: '@IDReporteHE' }, isArray: true }
                }
            ),
        GetDetaillForEmployee:
                $resource(SERVER+'/ExtraHours/ExtraHours/Detaill/:IDReporteHE/:IDEmpleado', { IDReporteHE: '@IDReporteHE', IDEmpleado : '@IDEmpleado' },
                {
                    "GetHorasExtraDetalle": { method: "GET", params: { IDReporteHE: '@IDReporteHE', IDEmpleado : '@IDEmpleado' }, isArray: true }
                }
            ),
        GetEmployees:
            $resource(SERVER+'/ExtraHours/ExtraHours/MyEmployees', {},
                {
                    "GetByApproverId": { method: "GET", params: { }, isArray: true }
                }
            ),
       	Create:
       		$resource(SERVER+'/ExtraHours/ExtraHours/Add', {},
	       		{
	       		    "AddExtraHour": { method: "POST", params: {}, isArray: false }
	       		}
	       	),
       	AddDetaill:
       		$resource(SERVER+'/ExtraHours/ExtraHours/Detaill/Add', {},
	       		{
	       		    "AddExtraHourDetaill": { method: "POST", params: {}, isArray: false }
	       		}
	       	),
       	Update:
            $resource(SERVER+'/ExtraHours/ExtraHours/Detaill/Update', {},
	       		{
	       		    "UpdateExtraHour": { method : "PUT", params: {}, isArray: false }
	       		}
	       	),
        Export:
            $resource(SERVER+'/ExtraHours/ExtraHours/GetFile', {},
                {
                    "GetFile": { method : "GET", params: {}}
                }
            ),
        RemoveDetaill:
            $resource(SERVER+'/ExtraHours/ExtraHours/Detaill/Remove/:iDHoraExtraDetalle', { iDHoraExtraDetalle : '@iDHoraExtraDetalle' },
                {
                    "Delete": { method : "DELETE", params: { iDHoraExtraDetalle : '@iDHoraExtraDetalle' }, isArray : false }
                }
            ),
       	//Delete:
        //    $resource(SERVER+'/ExtraHours/ExtraHours/Delete/:ScheduleId', { ScheduleId: '@ScheduleId' },
	    //   		{
	    //   		    "DeleteSchedule": { method: "Delete", params: { ScheduleId: '@ScheduleId' }, isArray: false }
	    //   		}
	    //   	)
    };

}]);