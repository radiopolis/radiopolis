payRollApp.factory('Areas', ['$resource', 'SERVER', function ($resource, SERVER) {

    return {
        Area:
            $resource(SERVER+'/ExtraHours/Areas', {},
                {
                    "GetAreas": { method: "GET", params: {}, isArray: true }
                }
            ),

       	add:
       		$resource(SERVER+'/ExtraHours/Areas/Add', {},
	       		{
	       			"AddArea": { method : "POST", params: {}, isArray: false }
	       		}
	       	),
        subAreas:
            $resource(SERVER+'/ExtraHours/SubAreas', {},
                {
                    "GetSubAreas": { method: "GET", params: {}, isArray: true }
                }
            ),
    };

}]);