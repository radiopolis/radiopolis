payRollApp.factory('Export', ['$http', 'SERVER', function ($http, SERVER) {

    var downloadSpreadsheet = {};

    downloadSpreadsheet.downloadIt = function (params, urlServer, name, isWord) {
        $http.get(SERVER + urlServer, {
            responseType: "arraybuffer",
            params: params
        }).success(function (data, status, headers) {

            var octetStreamMime = (isWord)? "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            // Get the headers
            headers = headers();

            // Get the filename from the x-filename header or default to "download.bin"
            var ext = (isWord)? '.docx' : '.xslx';
            var filename = name + ext;

            // Determine the content type from the header or default to "application/octet-stream"
            var contentType = octetStreamMime;

            if (navigator.msSaveBlob) {
                // Save blob is supported, so get the blob as it's contentType and call save.
                var blob = new Blob([data], { type: contentType });
                navigator.msSaveBlob(blob, filename);
            }
            else {
                // Get the blob url creator
                var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
                if (urlCreator) {
                    // Try to use a download link
                    var link = document.createElement("a");
                    if ("download" in link) {
                        // Prepare a blob URL
                        var blob = new Blob([data], { type: contentType });
                        var url = urlCreator.createObjectURL(blob);
                        link.setAttribute("href", url);

                        // Set the download attribute (Supported in Chrome 14+ / Firefox 20+)
                        link.setAttribute("download", filename);

                        // Simulate clicking the download link
                        var event = document.createEvent('MouseEvents');
                        event.initMouseEvent('click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
                        link.dispatchEvent(event);

                    } else {
                        // Prepare a blob URL
                        // Use application/octet-stream when using window.location to force download
                        var blob = new Blob([data], { type: octetStreamMime });
                        var url = urlCreator.createObjectURL(blob);
                        window.location = url;
                    }

                } else {
                    return "Not supported";
                }
            }


        })
        .error(function (data, status) {
            return "Request failed with status: " + status;
        });
    }
    return downloadSpreadsheet;
}]);