payRollApp.factory('Horarios', ['$resource', 'SERVER', function ($resource, SERVER) {

    return {
        Get:
            $resource(SERVER+'/ExtraHours/Schedules', {},
                {
                    "GetHorarios": { method: "GET", params: {}, isArray: true }
                }
            ),
       	Create:
       		$resource(SERVER+'/ExtraHours/Schedules/Add', {},
	       		{
	       			"AddHorario": { method : "POST", params: {}, isArray: false }
	       		}
	       	),
       	Update:
            $resource(SERVER+'/ExtraHours/Schedules/Update', {},
	       		{
	       		    "UpdateHorario": { method : "PUT", params: {}, isArray: false }
	       		}
	       	),
       	Delete:
            $resource(SERVER+'/ExtraHours/Schedules/Delete/:ScheduleId', { ScheduleId: '@ScheduleId' },
	       		{
	       		    "DeleteSchedule": { method: "DELETE", params: { ScheduleId: '@ScheduleId' }, isArray: false }
	       		}
	       	),
        SchedulesByEmployee:
            $resource(SERVER+'/ExtraHours/Schedules/Employee', {},
              {
                  "GetSchedules": { method: "GET", params: {}, isArray: true }
              }
            ),
    };

}]);