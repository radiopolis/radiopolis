payRollApp.factory('Login', ['$resource', 'SERVER', '$http', 'authService', function ($resource, SERVER, $http, authService) {
	return {
		Authenticate: function() {
      		return $http({
				  	method: 'POST',
				  	url: SERVER+'/ExtraHours/Login'
				}).then(
					function successCallback(response) {
						authService.loginConfirmed();
				  	},
				  	function errorCallback(response) {
				  		authService.loginCancelled();
				  	}
				);
		},

		Permissions:
            $resource(SERVER+'/ExtraHours/Login/Name', {},
	       		{
	       		    "GetUserPermissions": { method : "GET", params: {}, isArray: true }
	       		}
	       	),

	    Password :
	     	$resource(SERVER+'/ExtraHours/Login/ChangePass', {},
	       		{
	       		    "ChangePass": { method : "PUT", params: {}, isArray: false }
	       		}
	       	),
		ForgotPassword:
       		$resource(SERVER+'/ExtraHours/Login/ChangePass', {},
	       		{
	       		    "ChangePass": { method: "PUT", params: {}, isArray: false }
	       		}
	       	)
    };

}]);