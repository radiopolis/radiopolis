var payRollApp = angular.module('payRollApp', ['ngResource', 'ui.router', 'ui.bootstrap', 'ngRoute', 'ngGrid', 'angular-loading-bar', 'http-auth-interceptor', 'rad.http-auth'])

    .value("toastr", toastr)

    //  .constant('SERVER', 'http://localhost:55514/')
    .constant('SERVER', 'http://181.49.235.169:2121/')
	//.constant('SERVER', 'http://10.10.10.13:2122/')


    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/home');

        $.fn.datepicker.dates['es'] = {
            days: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
            daysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
            daysMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            today: 'Hoy'
        };


        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $stateProvider

        // Home
        .state('/home', {
            url: '/home',
            controller: 'homeCtrl',
            templateUrl: 'app/templates/home.html'
        })

        // Login
        .state('/', {
            url: '/',
            controller: 'loginCtrl',
            templateUrl: 'app/templates/login.html'
        })

        //Cruds

            //Areas
            .state('/manageAreas', {
                url: '/manageAreas',
                controller: 'manageAreas_Ctrl',
                templateUrl: 'app/templates/Cruds/manageAreas.html'
            })

            //CenterCosts
            .state('/manageCentersCost', {
                url: '/manageCentersCost',
                controller: 'manageCentersCost_Ctrl',
                templateUrl: 'app/templates/Cruds/manageCentersCost.html'
            })

        //Employees

            //Create
            .state('/createEmployee', {
                url: '/createEmployee',
                controller: 'createEmployee_Ctrl',
                templateUrl: 'app/templates/employees/createEmployee.html'
            })

            //Search
            .state('/searchEmployee', {
                url: '/searchEmployee',
                controller: 'searchEmployee_Ctrl',
                templateUrl: 'app/templates/employees/searchEmployee.html'
            })

             //Manage Schedules
            .state('/schedules', {
                url: '/schedules',
                controller: 'manageSchedules_Ctrl',
                templateUrl: 'app/templates/employees/manageSchedules.html'
            })


        //payRolls

            //Add
            .state('/addHours', {
                url: '/addHours',
                controller: 'addHours_Ctrl',
                templateUrl: 'app/templates/extraHours/addHours.html'
            })

            //Approve
            .state('/approveHours', {
                url: '/approveHours',
                controller: 'approveHours_Ctrl',
                templateUrl: 'app/templates/extraHours/approveHours.html'
            })

            //Detaill
            .state('/detaill', {
                url: '/detaill/:employeeId/:filter',
                controller: 'detaillHours_Ctrl',
                templateUrl: 'app/templates/extraHours/detaillHours.html'
            })

            //Liquidate
            .state('/liquidateHours', {
                url: '/liquidateHours',
                controller: 'liquidateHours_Ctrl',
                templateUrl: 'app/templates/extraHours/liquidateHours.html'
            })

            .state('/reportLiquidateHours', {
                url: '/reportLiquidateHours',
                controller: 'reportLiquidateHours_Ctrl',
                templateUrl: 'app/templates/extraHours/reportLiquidateHours.html'
            })


            .state('/forgotPassword', {
                url: '/forgotPassword',
                controller: 'forgotPassword_Ctrl',
                templateUrl: 'app/templates/forgotPassword.html'
            })                      

        // Requests
          // vacations
          .state('/vacations', {
              url: '/vacations',
              controller: 'vacations_Ctrl',
              templateUrl: 'app/templates/requests/vacations.html'
          })

        $httpProvider.interceptors.push('Interceptor');


    }])
