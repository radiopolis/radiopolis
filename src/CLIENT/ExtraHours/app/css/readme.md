﻿Manejo de estilos con Stylus
=========
----    

1. Instalar NodeJS
----
  - [Nodejs.org]
  
2. Instalar Stylus
----
  - En Terminal (o una ventana de Command Prompt (CMD)):
        npm install -g stylus

3. Modificar estilos
----
  - Navegar a la carpeta css/styl del proyecto. Cada archivo .styl hace referencia a una vista específica o a estilos generales para la aplicación.

        En stylus no se usan {}:;
    
    Por el contrario se usa la "indentación" (espaciado) para representar herencias entre propiedades y clases.

4. Añadir archivos .styl
----
  - Si se creó un archivo .styl adicional debe agregarse una nueva línea con una referencia a este dentro del archivo main.styl de la carpeta css/styl de la siguiente forma:

        @import 'NOMBRE_DEL_ARCHIVO'

5. Compilar CSS
----
  - Desde una Terminal navegar hasta la carpeta css/styl del proyecto y ejecutar lo siguiente:
            stylus main.styl --out ../

6. Hacer que Stylus compile cambios automáticamente
----
 - Para hacer que Stylus compile automáticamente los cambios realizados a un archivo .styl y genere un nuevo archivo .css se corre el siguiente comando en una Terminal:

        stylus main.styl --out ../ -w


Versión
----

1.0