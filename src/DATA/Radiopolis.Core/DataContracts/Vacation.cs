﻿namespace Radiopolis.Core.DataContracts
{
    public class Vacation
    {
       
        public Vacation(dynamic data)
        {
            Id = data.Id;
            IDEmpleado = data.IDEmpleado;
            Periodo = data.Periodo;
            Dias = data.Dias;
            Descripcion = data.Descripcion;
            DiasTomados = data.DiasTomados;
        }

        public Vacation() { }

        public int Id { get; set; }
        public int IDEmpleado { get; set; }
        public int Periodo { get; set; }
        public int Dias { get; set; }
        public string Descripcion { get; set; }
        public int DiasTomados { get; set; }

    }
}
