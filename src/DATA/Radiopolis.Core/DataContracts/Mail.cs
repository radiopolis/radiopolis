﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Radiopolis.Core.DataContracts
{
    public class Mail
    {
        public string toAddress { get; set; }
        public string CCBoss { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string BCCExtra { get; set; }
        public string Attachment { get; set; }
    }
}
