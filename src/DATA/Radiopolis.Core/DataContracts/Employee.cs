﻿using System;

namespace Radiopolis.Core.DataContracts
{
    public class Employee
    {
        public Employee(dynamic data)
        {
            IDEmpleado = data.IDEmpleado;
            IDJefe = data.IDJefe;
            IDCargo = data.IDCargo;
            IDArea = data.IDArea;
            IDSubArea = data.IDSubArea;
            IDEmpresa = data.IDEmpresa;
            IDCentro = data.IDCentro;
            IDSubCentro = data.IDSubCentro;
            IDContrato = data.IDContrato;

            cargoExtendido = data.cargoExtendido;
            Cedula = data.Cedula;
            Nombres = data.Nombres;
            Apellidos = data.Apellidos;
            FechaIngreso = data.FechaIngreso;
            FechaTerminacionContrato = data?.FechaTerminacionContrato;
            Telefono = data.Telefono;
            Movil = data.Movil;
            Email = data.Email;
            Estado = data.Estado;
            Genero = data.Genero;
            Prorroga = data.Prorroga;
            IDRol = data.IDRol;
        }

        public Employee()
        { }
        // ZONA IDS
        public int IDEmpleado { get; set; }
        public int IDJefe { get; set; }
        public int IDCargo { get; set; }
        public int IDArea { get; set; }
        public int IDSubArea { get; set; }
        public int IDEmpresa { get; set; }
        public int IDCentro { get; set; }
        public int IDSubCentro { get; set; }
        public int IDContrato { get; set; }
        public int IDRol { get; set; }
        // FIN ZONA IDS
        // ID Horarios
        public int[] IDHorario { get; set; }

        public string cargoExtendido { get; set; }
        public string Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime? FechaTerminacionContrato { get; set; }
        public string Telefono { get; set; }
        public string Movil { get; set; }
        public string Email { get; set; }
        public bool Estado { get; set; }
        public string Genero { get; set; }
        public string Prorroga { get; set; }
    }

    public class EmployeeExtended : Employee
    {
        public EmployeeExtended()
        {

        }

        public EmployeeExtended(dynamic data)
            : base(new Employee(data))
        {
            Cargo = data.Cargo;
            CentroCosto = data.CentroCosto;
            SubCentroCosto = data.SubCentroCosto;

            Empresa = data.Empresa;
            Area = data.Area;
            SubArea = data.SubArea;

        }

        public string Cargo { get; set; }
        public string CentroCosto { get; set; }
        public string SubCentroCosto { get; set; }
        public string Empresa { get; set; }
        public string Area { get; set; }
        public string SubArea { get; set; }
    }
}