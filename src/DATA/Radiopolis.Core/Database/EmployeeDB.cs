﻿using System.Collections.Generic;
using System.Linq;
using Simple.Data;
using Radiopolis.Core.DataContracts;

namespace Radiopolis.Core.DataBase
{
    public class EmployeeDB
    {
        public static IEnumerable<Employee> GetAllEmployees()
        {
            var retorno = new List<Employee>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno.OrderBy(x=> x.Apellidos);
        }

        public static Employee GetEmployeeById(int identification)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            int count = 0;

            var emple = new Employee(db.Empleados.All()
                .Where(db.Empleados.Cedula == identification).FirstOrDefault());

            var horario = db.EmpleadoHorarios.All()
                            .Where(db.EmpleadoHorarios.IDEmpleado == emple.IDEmpleado);

            emple.IDHorario = new int[horario.Count()];
            foreach (var item in horario)
            {
                emple.IDHorario[count] = item.IDHorario;
                count++;
            }


            return emple;
        }

        public static List<Employee> GetEmployeeById(List<int> ids)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                .Where(db.Empleados.IDEmpleado == ids).ToList();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }

        public static List<Employee> GetEmployeeByDocuments(List<string> numbers)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                .Where(db.Empleados.Cedula == numbers).ToList();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }

        public static dynamic UpdateEmployee(Employee employee)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            //toca actualizar los horarios tambien
            // eliminamos los horarios y volvemos a insertar
            db.EmpleadoHorarios.DeleteByIDEmpleado(employee.IDEmpleado);

            foreach (var item in employee.IDHorario) db.EmpleadoHorarios.Insert(IDEmpleado: employee.IDEmpleado, IDHorario: item);

            return db.Empleados.UpdateByIDEmpleado(employee);
        }

        public static List<Employee> GetEmployeesByScheduleID(int scheduleID)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");
            var data = db.Empleados.All()
                .Join(db.EmpleadoHorarios).On(db.EmpleadoHorarios.IDEmpleado == db.Empleados.IDEmpleado)
                .Where(db.EmpleadoHorarios.IDHorario == scheduleID);

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }
    }
}