﻿using System.Collections.Generic;
using System.Linq;
using Simple.Data;
using Radiopolis.Core.DataContracts;
using System;

namespace Radiopolis.Core.DataBase
{
    public class VacationDB
    {
        public static IEnumerable<Vacation> GetAll()
        {
            var retorno = new List<Vacation>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Vacaciones.All();

            foreach (var item in data) retorno.Add(new Vacation(item));

            return retorno;
        }

        public static void Create(List<Vacation> vacationsCreate)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            foreach (var item in vacationsCreate) db.Vacaciones.Insert(item);
        }

        public static void Update(List<Vacation> vacationsUpdate)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            foreach (var item in vacationsUpdate) db.Vacaciones.UpdateById(item);
        }
    }
}
