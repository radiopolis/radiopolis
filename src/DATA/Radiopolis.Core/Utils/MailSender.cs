﻿using NLog;
using Radiopolis.Core.DataContracts;
using Radiopolis.Core.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace Radiopolis.Core.Utils
{
    public class MailSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void Send(IEnumerable<Mail> mails)
        {
            try
            {
                foreach (var mail in mails)
                {
                    Console.WriteLine("se va enviar un mail: " + mail.CCBoss);
                    var fromAddress = new MailAddress(Settings.Default.MailTo);
                    var toAddress = new MailAddress(mail.toAddress);

                    var smtp = new SmtpClient
                    {
                        Host = Settings.Default.MailSMTP,
                        Port = Convert.ToInt16(Settings.Default.MailPort),
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, Settings.Default.MailPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = mail.Subject,
                        Body = mail.Body,
                        IsBodyHtml = true
                    })
                    {
                        if (!string.IsNullOrEmpty(mail.Attachment))
                        {
                            message.Attachments.Add(new Attachment(mail.Attachment));
                        }

                        if (!string.IsNullOrEmpty(mail.BCC))
                            message.Bcc.Add(mail.BCC);
                        if (!string.IsNullOrEmpty(mail.BCCExtra))
                            message.Bcc.Add(mail.BCCExtra);
                        smtp.Send(message);
                        Console.WriteLine("Mail Enviado!, con BCC: " + mail.BCC);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logger.Error(ex.ToString());
            }

        }


    }
}
