﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayrollDeductible.Properties;

namespace PayrollDeductible
{
    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            // Desprendibles de Nomina
            //-------------------------------------------
            // Se va a usar los desprendibles de nomina exportados por SIIGO luego se enviaran por mail 
            try
            {
                var job = new Job(Settings.Default.Prefix, Settings.Default.Folder, Settings.Default.Delimiter);
                job.Execute();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
    }
}
