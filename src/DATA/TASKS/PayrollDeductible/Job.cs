﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IronPdf;
using System.Web;
using Radiopolis.Core.DataBase;
using Radiopolis.Core.Utils;
using Radiopolis.Core.DataContracts;

namespace PayrollDeductible
{
    public class Job
    {
        private readonly string _prefix ;
        private readonly string _folder ;
        private readonly string _delimiter ;

        public Job(string prefix, string folder, string delimiter)
        {
            _prefix = prefix;
            _folder = folder;
            _delimiter = delimiter;
        }

        public void Execute()
        {
            var fileList = new DirectoryInfo(_folder).GetFiles($"*{_prefix}.txt", SearchOption.AllDirectories);
            var filesToDelete = new List<string>();
            var mails = new List<Mail>();

            foreach (var file in fileList)
            {
                string text = File.ReadAllText(file.FullName, Encoding.GetEncoding("ISO-8859-1"));
                filesToDelete.Add(file.FullName);

                var data = new List<Tuple<string, string>>();
                string[] desprendible = text.Split(new[] { _delimiter }, StringSplitOptions.None);

                foreach (var item in desprendible) data.Add(Tuple.Create(getBetween(item, "Cédula :", "Nombre :").Trim(), item));

                var employees = EmployeeDB.GetEmployeeByDocuments(data.Select(x => x.Item1).ToList()).ToList();

                foreach (var emp in employees)
                {
                    var info = data.Where(x => x.Item1 == emp.Cedula).FirstOrDefault();

                    if (info == null) continue;
                    if (string.IsNullOrEmpty(emp.Email)) continue;

                    HtmlToPdf HtmlToPdf = new HtmlToPdf();
                    HtmlToPdf.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Landscape;
                    PdfResource PDF = HtmlToPdf.RenderHtmlAsPdf(TextToHtml(info.Item2));
                    var fullName = _folder + @"\Pdf\" + emp.Cedula + "_" + DateTime.Now.ToString("ddMMyyyy") + ".Pdf";
                    filesToDelete.Add(fullName);

                    PDF.SaveAs(fullName);

                    mails.Add(new Mail {
                        toAddress = emp.Email,
                        Attachment = fullName,
                        Subject = "Desprendible de Nomina - " + DateTime.Now.ToString("MM/yyyy"),
                        Body = string.Empty
                    });
                }
            }

            if (mails.Any())
                MailSender.Send(mails);

            RemoveFiles(filesToDelete);
        }

        private static void RemoveFiles(List<string> files)
        {
            foreach (var file in files) File.Delete(file);
        }

        private static string TextToHtml(string text)
        {
            text = HttpUtility.HtmlEncode(text);
            text = text.Replace("\r\n", "\r");
            text = text.Replace("\n", "\r");
            text = text.Replace("\r", "<br>\r\n");
            text = text.Replace("  ", " &nbsp;");
            return text;
        }
        private static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }
    }
}
