﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VacationDays
{
    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            // Vacaciones
            //-------------------------------------------
            // Como no se puede calcular las vacaciones de los empleados por periodos toca incluir esta regla que se corra todos 
            // los días y vaya creando los periodos por año.
            try
            {
                var job = new Job();
                job.Execute(DateTime.Now);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

        }
    }
}
