﻿using Radiopolis.Core.DataBase;
using Radiopolis.Core.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace VacationDays
{
    internal class Job
    {
        public Job()
        {
        }

        public void Execute(DateTime now)
        {
            // formula de vacas -- dias desde la fecha/360*15
            var employees = EmployeeDB.GetAllEmployees();
            var vacations = VacationDB.GetAll();

            var vacationsCreate = new List<Vacation>();
            var vacationsUpdate = new List<Vacation>();


            foreach (var emple in employees)
            {
                // no es necesario crear todo el historico no tendria sentido todo debio ser cargado

                // tengo que validar si ya cumplio el año o no y segun eso creo el anterior periodo o creo uno nuevo y calculo eso

                // Antes que nada cargo todas los registros de vacaciones
                var vacacionesByEmple = vacations.Where(x => x.IDEmpleado == emple.IDEmpleado).ToList();

                var dateRaw = new DateTime(DateTime.Now.Year, emple.FechaIngreso.Month, emple.FechaIngreso.Day, DateTime.Now.Hour, DateTime.Now.Minute + 1, DateTime.Now.Second);

                // Aqui valido si la fecha de ingreso 
                if (dateRaw >= DateTime.Now)
                {
                    // mayor  
                    var vacNow = vacacionesByEmple.FirstOrDefault(g => g.Periodo == DateTime.Now.Year);

                    TimeSpan ts = DateTime.Now - new DateTime(DateTime.Now.Year - 1, emple.FechaIngreso.Month, emple.FechaIngreso.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second + 1);
                    var daysVactions = Convert.ToInt32(Math.Round(ts.Days / (double)360 * 15, MidpointRounding.ToEven));

                    if (vacNow == null) vacationsCreate.Add(new Vacation { Dias = daysVactions, DiasTomados = 0, IDEmpleado = emple.IDEmpleado, Periodo = DateTime.Now.Year });
                    else
                    {
                        vacNow.Dias = daysVactions;
                        vacationsUpdate.Add(vacNow);
                    }
                }
                else
                {
                    // menor  
                    // si no existe el perido pasado es un error creelo con sus días full
                    var vacNow = vacacionesByEmple.FirstOrDefault(g => g.Periodo == DateTime.Now.Year + 1);
                    TimeSpan ts = DateTime.Now - new DateTime(DateTime.Now.Year, emple.FechaIngreso.Month, emple.FechaIngreso.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second + 1);
                    var daysVactions = Convert.ToInt32(Math.Round(ts.Days / (double)360 * 15, MidpointRounding.ToEven));

                    if (vacNow == null) vacationsCreate.Add(new Vacation { Dias = daysVactions, DiasTomados = 0, IDEmpleado = emple.IDEmpleado, Periodo = DateTime.Now.Year + 1 });
                }

            }

            #region Old
            //foreach (var emple in employees)
            //{
            //    var periodo = now.Year;
            //    var year = now.Year;

            //    // Asignamos la fecha de ingreso como si fuera este año
            //    var dateEmployeeNow = new DateTime(now.Year, emple.FechaIngreso.Month, emple.FechaIngreso.Day, now.Hour, now.Minute, now.Second +1);

            //    // Si la fecha ya paso 
            //    if (dateEmployeeNow > now) year--;

            //    // Difference in days, hours, and minutes.
            //    TimeSpan ts = now - new DateTime(year, emple.FechaIngreso.Month, emple.FechaIngreso.Day, now.Hour, now.Minute, now.Second + 1);

            //    var vacation = vacations.Where(x => x.IDEmpleado == emple.IDEmpleado).ToList();

            //    // Difference in days.
            //    var daysVactions = Convert.ToInt32( Math.Round(((double)ts.Days / (double)360) * 15, MidpointRounding.ToEven));

            //    if (ts.Days == 0 && dateEmployeeNow.Day == now.Day)
            //        periodo = now.Year + 1;

            //    if (!vacation.Any())
            //    {
            //        if (periodo != now.Year + 1)
            //            periodo = now.Year + 1;
            //        // si no tiene ningun registro debe ser nuevo creelo
            //        vacationsCreate.Add(new Vacation { Dias = daysVactions, Descripcion = "Fisrt Date", DiasTomados = 0, IDEmpleado = emple.IDEmpleado, Periodo = periodo });
            //    }else
            //    {
            //        if (ts.Days == 0 && dateEmployeeNow.Day == now.Day)
            //            periodo = now.Year + 1;
            //        else
            //            periodo = vacation.Max(x => x.Periodo);

            //        // tengo que validar que no exista otro registro
            //        var newTerm = vacation.Where(x => x.Periodo == periodo).FirstOrDefault();
            //        if (newTerm == null)
            //            vacationsCreate.Add(new Vacation { Dias = daysVactions, Descripcion = "New Term", DiasTomados = 0, IDEmpleado = emple.IDEmpleado, Periodo = periodo });
            //        else
            //            // aqui se actualiza el dato de los días
            //            vacationsUpdate.Add(new Vacation { Dias = daysVactions, Descripcion = newTerm.Descripcion, DiasTomados = newTerm.DiasTomados, IDEmpleado = emple.IDEmpleado, Periodo = periodo });
            //    }
            //}
            #endregion            

            // cree y actualice lo nuevo
            if (vacationsCreate.Any())
                VacationDB.Create(vacationsCreate);
            if (vacationsUpdate.Any())
                VacationDB.Update(vacationsUpdate);
        }
    }
}