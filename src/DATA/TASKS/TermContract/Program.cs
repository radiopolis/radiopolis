﻿using NLog;
using System;
using System.Collections.Generic;

namespace TermContract
{
    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            // Terminación de Contratos
            //-------------------------------------------
            // Basicamente se tiene que traer todos los empleados y validar su tipo de contrato y sus configuraciones,
            // para poder notificar por correo mediante unas plantillas el vencimiento del mismo
            try
            {
                var job = new Job();
                job.Execute();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

        }
    }
}
