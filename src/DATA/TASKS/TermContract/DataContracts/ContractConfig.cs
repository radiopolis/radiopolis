﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TermContract.DataContracts
{
    public class ContractConfig
    {
        public int IDContrato { get; set; }

        public string Description { get; set; }

        public int DaysAlert { get; set; }

        public ContractConfig(dynamic data)
        {
            IDContrato = data.IDContrato;
            Description = data.Description;
            DaysAlert = data.DaysAlert;
        }
    }
}
