﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TermContract.DataContracts;
using TermContract.Properties;


namespace TermContract.Utils
{
    public static class MailSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static void Send(IEnumerable<Mail> mails)
        {
            try
            {
                foreach (var mail in mails)
                {
                    Console.WriteLine("se va enviar un mail: "+ mail.CCBoss);
                    var fromAddress = new MailAddress(Settings.Default.MailTo);
                    var toAddress = new MailAddress(mail.CCBoss);
                    var BCC = new MailAddress(mail.BCC);

                    var smtp = new SmtpClient
                    {
                        Host = Settings.Default.MailSMTP,
                        Port = Convert.ToInt16(Settings.Default.MailPort),
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, Settings.Default.MailPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = mail.Subject,
                        Body = mail.Body,
                        IsBodyHtml = true
                    })
                    {
                        message.Bcc.Add(BCC);
                        if (!string.IsNullOrEmpty(mail.BCCExtra))
                            message.Bcc.Add(mail.BCCExtra);
                        smtp.Send(message);
                        Console.WriteLine("Mail Enviado!, con BCC: " + BCC.Address);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                logger.Error(ex.ToString());
            }

        }
    }
}
