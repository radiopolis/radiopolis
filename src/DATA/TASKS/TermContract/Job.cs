﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TermContract.DataBase;
using TermContract.DataContracts;
using TermContract.Properties;
using TermContract.Utils;

namespace TermContract
{
    public class Job
    {
        private int _dayLast = 3;
        public void Execute()
        {
            //primero debo traer la configuración de los contratos a evaluar
            var contractConfig = ContractDB.GetAllContractsConfig();

            // se traen todos los empleados  
            var employeesRaw = EmployeeDB.GetAllEmployees();
            Console.WriteLine("dataconfig :" + contractConfig.Count() + " empleados : " + employeesRaw.Count());
            #region Test
            //var contractConfig = new List<ContractConfig>();

            //contractConfig.Add(new ContractConfig { DaysAlert = 10, IDContrato = 1 });
            //contractConfig.Add(new ContractConfig { DaysAlert = 5, IDContrato = 2 });
            //contractConfig.Add(new ContractConfig { DaysAlert = 3, IDContrato = 1 });

            //var employeesRaw = new List<Employee>();

            //employeesRaw.Add(new Employee { IDContrato  = 1, IDJefe = 0, IDEmpleado = 1, Nombres = "test" , Email ="gpineros@equitel.com.co", FechaTerminacionContrato = DateTime.Now.AddDays(4)});
            //employeesRaw.Add(new Employee { IDContrato = 2, IDJefe = 1, IDEmpleado = 2, Nombres = "test2" , Email = "test2@test.com", FechaTerminacionContrato = DateTime.Now.AddDays(6) });
            //employeesRaw.Add(new Employee { IDContrato = 3, IDJefe = 2, IDEmpleado = 3, Nombres = "test3" , Email = "test3@test.com" });
            //employeesRaw.Add(new Employee { IDContrato = 4, IDJefe = 3, IDEmpleado = 4, Nombres = "test4", Email = "test4@test.com" });
            #endregion
            // se saca listado de empleados basado en los contratos configurados
            var employees = employeesRaw.Where(x => contractConfig.Distinct().Any(y=> y.IDContrato == x.IDContrato)).ToList();

            var mailsToSend = new List<Mail>();

            foreach (var data in employees)
            {
                // se carga configuración segun el contrato, pueden ser varios
                var config = contractConfig.Where(x => x.IDContrato == data.IDContrato).ToList();

                // se crea una variable adicional para poder anexar en el asunto que el jefe no tiene correo
                var subjectExtra = string.Empty;

                // se suma un día por un fix que hay de horas
                var daysDiffEndContract = (data.FechaTerminacionContrato - DateTime.Now).Days + 1;

                var mailBoss = employeesRaw.Where(x => x.IDEmpleado == data.IDJefe).Select(x => x.Email).FirstOrDefault();

                if (mailBoss == null)
                {
                    subjectExtra = " - {El Jefe No tiene correo!}";
                    mailBoss = Settings.Default.MailArea;
                }
                foreach (var item in config) if (item.DaysAlert == daysDiffEndContract) mailsToSend.Add(ConvertMail("SendMail.html", "Alerta se va a vencer el Contrato de " + data.NombreCompleto() + subjectExtra, data, mailBoss));

                // si al final no se encuentra ninguna fecha a vencer, validemos una ultima de 3 días antes de vencer
                if (_dayLast == daysDiffEndContract) mailsToSend.Add(ConvertMail("SendLastMail.html","Urgente quedan '" + _dayLast + "' días para vencerse el Contrato de " + data.NombreCompleto() + subjectExtra, data, mailBoss));
            }
            Console.WriteLine("Mail a enviar : " + mailsToSend.Count());
            if (mailsToSend.Any()) MailSender.Send(mailsToSend);
        }

        private Mail ConvertMail(string template,string subject, Employee employee, string mailBoss)
        {
            // se tiene que buscar el mail del jefe
            return new Mail {
                BCC = Settings.Default.MailArea,
                CCBoss = mailBoss,
                Subject = subject,
                // aqui se hace esto para los empleados santa costilla ya que requieren correo adicional
                BCCExtra = employee.IDEmpresa == 4 ? "direccion-ayb@santacostilla.co" : "",
                Body = GetTemplate(template, employee)
            };
        }

        private string GetTemplate(string template, Employee employee)
        {
            string text;
            var fileStream = new FileStream(@"C:\Task\TermContract\MailTemplate\" + template, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8)) text = streamReader.ReadToEnd();
            text = text.Replace("@Nombres", employee.Nombres);
            text = text.Replace("@Apellidos", employee.Apellidos);
            text = text.Replace("@Cedula", employee.Cedula);
            text = text.Replace("@FechaIngreso", employee.FechaIngreso.ToShortDateString());
            text = text.Replace("@FechaFinalizacion", employee.FechaTerminacionContrato.ToShortDateString());
            text = text.Replace("@CargoExtendido", employee.cargoExtendido);
            text = text.Replace("@Prorroga", employee.Prorroga);
            return text;
        }
    }
}
