﻿using Simple.Data;
using System.Collections.Generic;
using TermContract.DataContracts;
using TermContract.Properties;

namespace TermContract.DataBase
{
    public class EmployeeDB
    {
        public static List<Employee> GetAllEmployees()
        {
            var retorno = new List<Employee>();
            var db = Database.OpenConnection(@"Data Source=PWBL1SQL\SQLWEB;Initial Catalog=NominaV;User ID=sa;Password=Radiopolis1019;Connect Timeout=180");

            var data = db.Empleados.All()
                                    .Where(db.Empleados.Estado == true).ToList();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }
    }
}
