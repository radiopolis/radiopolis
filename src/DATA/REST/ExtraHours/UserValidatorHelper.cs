﻿using Nancy.Authentication.Basic;
using System;
using System.Linq;
using Nancy.Security;
using ExtraHours.DataBase;

namespace ExtraHours
{
    public class UserValidatorHelper : IUserValidator
    {
        public IUserIdentity Validate(string username, string password)
        {
            var user = UserIdentityDB.GetUserByCredentials(username, password);
            if (user != null)
                return user;

            return null;
        }
    }
}