﻿using System;
using ExtraHours.DataBase;
using ExtraHours.Models;
using Nancy;
using Nancy.ModelBinding;
using ExtraHours.Services;
using Nancy.Responses;
using System.IO;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class ExtraHoursModule : NancyModule
    {
        public ExtraHoursModule()
            : base("ExtraHours/ExtraHours")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                //var loggedUser = 7; //TODO: implementar Id de sesión
                var loggedUser = ((UserIdentity)Context.CurrentUser).IDEmpleado;
                return JsonSerializer.SerializeObject(ExtraHourDB.GetAllExtraHoursByEMployeeID(loggedUser));
            };

            Get["/DetaillFiltered/{employeeId}/{filter}"] = parameters =>
            {
                var employeeId = (int)parameters.employeeId;
                var filter = (string)parameters.filter;

                return JsonSerializer.SerializeObject(ExtraHourDB.GetEmployeeExtraHours(employeeId, filter));
            };

            Get["/Detaill/{IDReporteHE}"] = parameters =>
            {
                var IDReporteHE = (int)parameters.IDReporteHE;
                var user =((UserIdentity)Context.CurrentUser).IDEmpleado;

                return JsonSerializer.SerializeObject(ExtraHourDB.GetExtraHourDetaillByIdReport(IDReporteHE, user));
            };

            Get["/Detaill/{IDReporteHE}/{IDEmpleado}"] = parameters =>
            {
                var IDReporteHE = (int)parameters.IDReporteHE;
                var user = (int)parameters.IDEmpleado;

                return JsonSerializer.SerializeObject(ExtraHourDB.GetExtraHourDetaillByIdReport(IDReporteHE, user));
            };

            Get["/MyEmployees"] = parameters =>
            {
                //var IdJefe = (int)6; //TODO: implementar Id de sesión
                var IdJefe = ((UserIdentity)Context.CurrentUser).IDEmpleado;
                return JsonSerializer.SerializeObject(ExtraHourDB.GetEmployeesByApprover(IdJefe));
            };
            
            Post["/Detaill/Add"] = parameters =>
            {
               var horaExtraDetalle = this.Bind<ExtraHourDetaill>();
               var userId = ((UserIdentity)Context.CurrentUser).IDEmpleado;

               ExtraHourDB.CreateExtraHourDetaill(horaExtraDetalle, userId);
               return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Detaill/Update"] = parameters =>
            {
                var horaExtraDetalle = this.Bind<ExtraHourDetailExtended>();

                ExtraHourDB.UpdateExtraHourDetail(horaExtraDetalle);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Delete["/Detaill/Remove/{iDHoraExtraDetalle}"] = parameters =>
            {
                var id = parameters.iDHoraExtraDetalle;
                ExtraHourDB.RemoveExtraHourDetail(id);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Get["/GetFile"] = parameters =>
            {
                //var data = this.Bind<InputRequest>();

                //var userId = 1; // user autenticado
                var userId = ((UserIdentity)Context.CurrentUser).IDEmpleado;
                if (Request.Query.InitialDate == null || Request.Query.FinalDate == null) return new Response().WithStatusCode(HttpStatusCode.BadRequest);

                var file = ExtraHoursService.GetFileExcel(Request.Query.InitialDate, Request.Query.FinalDate);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("HorasExtras_" + userId + "_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_") + ".xlsx");

            };

            Get["/Detaill/GetFile"] = parameters =>
            {

                //var userId = 1; // user autenticado
                var userId = ((UserIdentity)Context.CurrentUser).IDEmpleado;
                if (Request.Query.InitialDate == null || Request.Query.FinalDate == null) return new Response().WithStatusCode(HttpStatusCode.BadRequest);

                var file = ExtraHoursService.GetDetaillFileExcel(Request.Query.InitialDate, Request.Query.FinalDate);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("HorasExtrasDetalle_" + userId + "_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_") + ".xlsx");

            };

        }
    }
}