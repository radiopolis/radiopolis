﻿using ExtraHours.DataBase;
using Nancy;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class ContractsModule : NancyModule
    {
        public ContractsModule()
            : base("ExtraHours/Contracts")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                return JsonSerializer.SerializeObject(ContractDB.GetAllContracts());
            };
        }
    }
}