﻿using ExtraHours.DataBase;
using Nancy;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class AreasModule : NancyModule
    {
        public AreasModule()
            : base("ExtraHours/")
        {
            this.RequiresAuthentication();
            //this.Context.CurrentUser.Identity.Name;
            Get["/Areas"] = parameters =>
            {
                return JsonSerializer.SerializeObject(AreaDB.GetAllAreas());
            };

            Get["/SubAreas"] = parameters =>
            {
                return JsonSerializer.SerializeObject(AreaDB.GetAllSubAreas());
            };
        }
    }
}