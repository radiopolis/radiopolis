﻿using ExtraHours.DataBase;
using ExtraHours.Models;
using ExtraHours.Services;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using Nancy.Security;
using System;

namespace ExtraHours.Modules
{
    public class EmployeesModule : NancyModule
    {
        public EmployeesModule()
            : base("ExtraHours/Employees")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                return JsonSerializer.SerializeObject(EmployeeDB.GetAllEmployees());
            };

            Post["/Add"] = parameters =>
            {
                var employee = this.Bind<Employee>();

                Employee CreateEmployee = EmployeeDB.CreateEmployee(employee);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Update"] = parameters =>
            {
                var employee = this.Bind<Employee>();

                var result = EmployeeDB.UpdateEmployee(employee);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Get["/Company"] = parameters =>
            {
                return JsonSerializer.SerializeObject(EmployeeDB.GetAllCompanies());
            };

            Get["/{Identification}"] = parameters =>
            {
                var employeeid = (int)parameters.Identification;

                return JsonSerializer.SerializeObject(EmployeeDB.GetEmployeeByIdentificationSchedule(employeeid));
            };

            Get["/ScheduleId/{ScheduleId}"] = parameters =>
            {
                var scheduleId = (int)parameters.ScheduleId;

                return JsonSerializer.SerializeObject(EmployeeDB.GetEmployeesByScheduleID(scheduleId));
            };

            Get["/GetFile"] = parameters =>
            {
                var userId = ((UserIdentity)Context.CurrentUser).IDEmpleado;
                var file = EmployeeService.GetFileExcel(Request.Query.InitialDate, Request.Query.FinalDate);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("Empleados_" + userId + "_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_") + ".xlsx");

            };
        }
    }
}