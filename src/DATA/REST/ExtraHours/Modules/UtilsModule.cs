﻿using ExtraHours.DataBase;
using Nancy;
using System.Linq;

namespace ExtraHours.Modules
{
    public class UtilsModule : NancyModule
    {
        public UtilsModule()
            : base("ExtraHours/Utils")
        {
            //this.RequiresAuthentication();

            Get["/Holidays"] = parameters =>
            {
                return JsonSerializer.SerializeObject(UtilsDB.GetHolidays().Select(x => x.ToString("dd/MM/yyyy")));
            };
        }
    }
}