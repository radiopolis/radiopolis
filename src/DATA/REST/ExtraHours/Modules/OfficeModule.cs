﻿using ExtraHours.DataBase;
using Nancy;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class OfficeModule : NancyModule
    {
        public OfficeModule()
            : base("ExtraHours/Office")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                return JsonSerializer.SerializeObject(EmployeeDB.GetAllOffice());
            };
        }
    }
}