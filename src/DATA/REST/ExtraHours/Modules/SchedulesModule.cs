﻿using ExtraHours.DataBase;
using ExtraHours.Models;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class SchedulesModule : NancyModule
    {
        public SchedulesModule()
            : base("ExtraHours/Schedules")
        {
            this.RequiresAuthentication();
            Get["/"] = parameters =>
            {
                return JsonSerializer.SerializeObject(HorarioDB.GetAllHorarios());
            };

            Get["/Employee"] = parameters =>
            {
                var employeeId = ((UserIdentity)Context.CurrentUser).IDEmpleado;

                return JsonSerializer.SerializeObject(HorarioDB.GetScheduleByEmployee(employeeId));
            };

            Post["/Add"] = parameters =>
            {
                var horario = this.Bind<Horario>();

                HorarioDB.CreateHorario(horario);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Update"] = parameters =>
            {
                var horario = this.Bind<Horario>();

                HorarioDB.UpdateHorario(horario);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Delete["/Delete/{ScheduleId}"] = parameters =>
            {
                var scheduleId = (int)parameters.ScheduleId;
                HorarioDB.DeleteSchedule(scheduleId);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}