﻿using ExtraHours.DataBase;
using Nancy;
using Nancy.Security;

namespace ExtraHours.Modules
{
    public class CostCenterModule : NancyModule
    {
        public CostCenterModule()
            : base("ExtraHours/")
        {
            this.RequiresAuthentication();
            Get["/CostCenter"] = parameters =>
            {
                return JsonSerializer.SerializeObject(CostCenterDB.GetAllCostCenter());
            };

            Get["/SubCostCenter/{Id}"] = parameters =>
            {
                var costCenterId = (int)parameters.Id;

                return JsonSerializer.SerializeObject(CostCenterDB.GetAllSubCostCenter(costCenterId));
            };

            //TODO: pendiente agregar los metodos faltantes (UPD, ADD, DEL)
        }
    }
}