﻿using ExtraHours.DataBase;
using ExtraHours.Models;
using Nancy;
using Nancy.Security;
using Nancy.ModelBinding;

namespace ExtraHours.Modules
{
    public class UserIdentityModule : NancyModule
    {
        public UserIdentityModule()
            : base("ExtraHours/")
        {
            this.RequiresAuthentication();
            Post["/Login"] = _ =>
            {
                return new Response { StatusCode = HttpStatusCode.Accepted };
            };

            Get["/Login/Name"] = _ =>
            {
                dynamic[] data = new dynamic[] { ((UserIdentity)Context.CurrentUser).Acceso, ((UserIdentity)Context.CurrentUser).Name, ((UserIdentity)Context.CurrentUser).SubArea };
                return data;
            };


        }
    }
}