﻿using ExtraHours.DataBase;
using ExtraHours.Models;
using ExtraHours.Services;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Modules
{
    public class RequestModule :  NancyModule
    {
        public RequestModule()
             : base("ExtraHours/Request")
        {
            this.RequiresAuthentication();

            // VACACIONES
            Get["/Vacations/AvalaibleDays"] = parameters =>
            {
                var employeeId = ((UserIdentity)Context.CurrentUser).IDEmpleado;

                return JsonSerializer.SerializeObject(ResquestDB.GetPeriods(employeeId));
            };

            Delete["/Vacations/Delete/{Id}"] = parameters =>
            {
                var id = parameters.Id;
                ResquestDB.RemoveRequest(id);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/Vacations/Add"] = parameters =>
            {
                var vacation = this.Bind<RequestVacation>();
                var userIdentify = ((UserIdentity)Context.CurrentUser);

                ResquestDB.CreateVacation(vacation, userIdentify);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };


            Put["/Vacations/Update/{Id}"] = parameters =>
            {
                var vacation = this.Bind<RequestVacation>();
                var userIdentify = ((UserIdentity)Context.CurrentUser);
                var id = (int)parameters.Id;

                ResquestDB.UpdateVacation(id, vacation, userIdentify);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/ChangeStatus"] = parameters =>
            {
                var requests = this.Bind<List<RequestStatus>>();
                var userIdentify = ((UserIdentity)Context.CurrentUser);

                ResquestDB.UpdateStatusRequest(requests, userIdentify);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Get["/My/{Type}"] = parameters =>
            {
                var userIdentify = ((UserIdentity)Context.CurrentUser);
                var type = (int)parameters.Type;

                return JsonSerializer.SerializeObject(ResquestDB.GetRequestByTypeEmployee(type, userIdentify));
            };

            Get["/GetFile/{Id}"] = parameters =>
            {
                var userIdentify = ((UserIdentity)Context.CurrentUser);
                
                var id = (int)parameters.Id;

                var nameDoc = "DocVacation_" + userIdentify.Name + "_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_").Replace("/","-") + ".docx";
                //var nameDoc = "DocVacation_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_").Replace("/","-") + ".docx";

                var file = UtilsService.GetFileDocVacation(id, nameDoc);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                return response.AsAttachment(nameDoc);
            };

            Get["/Area/{Area}/{Type}/{Status}"] = parameters =>
            {
                var userIdentify = ((UserIdentity)Context.CurrentUser);
                var type = (int)parameters.Type;
                var area = (int)parameters.Area;
                var status = (string)parameters.Status;

                return JsonSerializer.SerializeObject(ResquestDB.GetRequestByAreaTypeStatus(area, type, status , userIdentify));
            };

            Get["/Trace/{Id}"] = parameters =>
            {
                var id = (int)parameters.Id;

                return JsonSerializer.SerializeObject(ResquestDB.GetTraceRequestById(id));
            };

            Get["/Approve/{Type}/{Status}"] = parameters =>
            {
                var userIdentify = ((UserIdentity)Context.CurrentUser);
                var type = (int)parameters.Type;
                var status = (string)parameters.Status;

                return JsonSerializer.SerializeObject(ResquestDB.GetRequestByTypeBoss(type, userIdentify, status));
            };

        }
    }
}