﻿using ExtraHours.Services;
using Nancy;
using Nancy.Responses;
using System;
using System.IO;

namespace ExtraHours.Modules
{
    public class ModuleTest : NancyModule
    {
        public ModuleTest()
            : base("/Test")
        {
            Get["/"] = _ =>
            {
                var file = ExtraHoursService.GetFileExcel(new DateTime(2016,02, 01,0,0,0), new DateTime(2016,02,15,23,59,59));
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("HorasExtras_1_" + DateTime.UtcNow.ToShortDateString().Replace(" ", "_") + ".xlsx");

            };
        }
    }
}