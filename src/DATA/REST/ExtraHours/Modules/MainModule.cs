﻿using Nancy;
using ExtraHours.Models;
using Nancy.ModelBinding;
using ExtraHours.DataBase;
using ExtraHours.Services;

namespace ExtraHours.Modules
{
    public class MainModule : NancyModule
    {
        public MainModule()
        {
            Get["/"] = parameters => View["index"];

            Put["/ExtraHours/Login/ChangePass"] = parameters =>
            {
                var data = this.Bind<UserIdentity>();

                var user = EmployeeDB.GetUserData(data.UserName);

                if (user == null) return new Response { StatusCode = HttpStatusCode.Unauthorized };

                ExtraHoursService.SendMailPassword(user);

                return new Response { StatusCode = HttpStatusCode.Accepted };
            };
        }
    }
}