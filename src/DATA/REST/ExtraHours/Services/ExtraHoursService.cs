﻿using ExtraHours.DataBase;
using ExtraHours.Helpers;
using ExtraHours.Models;
using OfficeOpenXml;
using Radiopolis.Core.DataContracts;
using Radiopolis.Core.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;


namespace ExtraHours.Services
{
    public static class ExtraHoursService
    {
        public static Stream GetFileExcel(DateTime dateIniRaw, DateTime dateEndRaw)
        {
            try
            {
                DateTime dateIni = new DateTime(dateIniRaw.Year, dateIniRaw.Month, dateIniRaw.Day, 0, 0, 0);
                DateTime dateEnd = new DateTime(dateEndRaw.Year, dateEndRaw.Month, dateEndRaw.Day, 23, 59, 59);

                // datos listos para formar excel
                var data = ProcessLiquidateHours(dateIni, dateEnd);

                // proceso de llenado de excel esto es lo facil
                var headers = new List<object[]>();

                headers.Add(ExcelHelper.GetSpreadSheetHeader());

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte HorasExtra");
                    pck.Workbook.Properties.Author = "HorasExtrasWWW-GPR";

                    var recordsDecoded = new List<object[]>();

                    foreach (var da in data) recordsDecoded.Add(ExcelHelper.ConvertExtraHourFileToObject(da));

                    ws.Cells["A1"].Value = "Reporte Horas Extras";
                    ws.Cells["A1"].Style.Font.Bold = true;

                    ws.Cells["A3"].Value = "Fecha Inicial : ";
                    ws.Cells["A3"].Style.Font.Bold = true;
                    ws.Cells["B3"].Value = dateIni.ToShortDateString();
                    ws.Cells["A4"].Value = "Fecha Final : ";
                    ws.Cells["A4"].Style.Font.Bold = true;
                    ws.Cells["B4"].Value = dateEnd.ToShortDateString();

                    ws.Cells["A6"].LoadFromArrays(headers);
                    ws.Cells["A6:O6"].Style.Font.Bold = true;
                    ws.Cells["A6:O6"].AutoFitColumns();

                    if (recordsDecoded.Any())
                        ws.Cells["A7"].LoadFromArrays(recordsDecoded);

                    return new MemoryStream(pck.GetAsByteArray());
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static Stream GetDetaillFileExcel(DateTime dateIniRaw, DateTime dateEndRaw)
        {
            try
            {
                DateTime dateInit = new DateTime(dateIniRaw.Year, dateIniRaw.Month, dateIniRaw.Day, 0, 0, 0);
                DateTime dateEnd = new DateTime(dateEndRaw.Year, dateEndRaw.Month, dateEndRaw.Day, 23, 59, 59);
                // datos listos para formar excel
                var data = ExtraHourDB.GetAllExtraHourDetaillByDates(dateInit, dateEnd);

                // proceso de llenado de excel esto es lo facil
                var headers = new List<object[]>();

                headers.Add(ExcelHelper.GetSpreadSheetDetaillHeader());

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte HorasExtra");
                    pck.Workbook.Properties.Author = "HorasExtrasWWW-GPR";

                    var recordsDecoded = new List<object[]>();

                    var horarios = HorarioDB.GetAllHorarios();

                    var empleados = EmployeeDB.GetAllEmployees();

                    foreach (var da in data)recordsDecoded.Add(ExcelHelper.ConvertDetaillExtraHourFileToObject(da, empleados.Where(x=> x.IDEmpleado == da.IDEmpleado).Select(x=> Tuple.Create(x.Cedula, x.Nombres + " " + x.Apellidos)).FirstOrDefault(), horarios.Where(x=> x.IDHorario == da.IDHorario).Select(x=> x.Descripcion).FirstOrDefault()));

                    ws.Cells["A1"].Value = "Reporte Detallado Horas Extras";
                    ws.Cells["A1"].Style.Font.Bold = true;

                    ws.Cells["A3"].Value = "Fecha Inicial : ";
                    ws.Cells["A3"].Style.Font.Bold = true;
                    ws.Cells["B3"].Value = dateInit.ToShortDateString();
                    ws.Cells["A4"].Value = "Fecha Final : ";
                    ws.Cells["A4"].Style.Font.Bold = true;
                    ws.Cells["B4"].Value = dateEnd.ToShortDateString();

                    ws.Cells["A6"].LoadFromArrays(headers);
                    ws.Cells["A6:O6"].Style.Font.Bold = true;
                    ws.Cells["A6:O6"].AutoFitColumns();

                    if (recordsDecoded.Any())
                        ws.Cells["A7"].LoadFromArrays(recordsDecoded);

                    return new MemoryStream(pck.GetAsByteArray());
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        // TRAE TODAS LAS COSAS PARA ARMAR LA CABECERA DEL PROCESO
        public static IEnumerable<ExtraHourFile> ProcessLiquidateHours(DateTime dateIni, DateTime dateEnd)
        {
            // objeto de retorno
            var dataExcel = new List<ExtraHourFile>();

            // se trae los festivos
            var dataHolidays = UtilsDB.GetHolidaysByDates(dateIni, dateEnd);
            //listado de horarios
            var listSchedules = HorarioDB.GetAllHorarios();

            var execute = new WorkdayProcess(dataHolidays, listSchedules);

            // se debe traer el detalle de las horas extras basadas en la fecha de aprobacion de jefe

            // **listado de el detalle
            // aqui hay que tener cuidado ya que pueden aprobar horas extras de otros meses, ya que la fecha que se usa es la de
            // aprobacion mas no la de generacion
            var listExtraHourDetail = ExtraHourDB.GetExtraHourDetaillByDates(dateIni, dateEnd);

            var dataEmployee = EmployeeDB.GetEmployeesExtendedById(listExtraHourDetail.Select(x => x.IDEmpleado).Distinct().ToList());

            foreach (var employee in dataEmployee)
            {
                var dataRaw = listExtraHourDetail.Where(x=> x.IDEmpleado == employee.IDEmpleado).ToList();
                dataExcel.Add(execute.ProcessWordTimeByEmployee(employee, dataRaw.Select(c=> Tuple.Create(c.FechaEntrada,c.FechaSalida, c.IDHorario)).ToList()));
            }

            foreach (var raw in listExtraHourDetail) raw.Estado = "L";

            //Debe cambiar el estado de todos estos detalles de horas extra ya que se liquidaron
            foreach (var data in listExtraHourDetail) ExtraHourDB.UpdateStatusExtraHourDetail(data.IDHoraExtraDetalle.Value, "L");

           return dataExcel.OrderBy(x=> x.Apellidos);

        }

        // PROCESAMIENTO AL DETALLE
        private static void GetHoursByType(List<ExtraHourFile> dataExcel,int idEmpleado, DateTime dateInit, DateTime dateEnd, List<Tuple<int, int>> listHours, List<Tuple<int, int>> scheduleEntrada, List<Tuple<int, int>> scheduleSalida, List<DateTime> dataHolidays,List<string> listDaysWork)
        {
            var listExtraData = ExtraHourData.Get();

            var dataEmployeeRaw = dataExcel.Where(x => x.IDEmpleado == idEmpleado).FirstOrDefault();

            int index = 0;
            int length = 1;
            // todo esto se hace por que hay dos fechas las cuales pueden corresponder a dos tipos de días diferentes y puedo aplicar horas extras con otro conte
            bool validateDay;
            bool validateWork;
            var listHoursRaw = new List<Tuple<int, int>>();
            var scheduleRaw = new List<Tuple<int, int>>();
            var currentDate = new DateTime();

            // validacion para fechas de un día para otro
            if (dateInit.ToString("MM-dd-yy") != dateEnd.ToString("MM-dd-yy"))
            {
                index = listHours.FindIndex(x => x.Item1 == 0);
                length = 2;
            }

            // por defecto usaremos la fecha ini para suponer que las fechas son las mismas
            currentDate = dateInit;
            scheduleRaw = scheduleEntrada;

            foreach (var listExtra in listExtraData)
            {
                for (int i = 1; i <= length; i++)
                {
                    // quiere decir que se evaluaron las 2 fechas
                    if (i == 2)
                    {
                        // aqui se asignan datos para la fecha final
                        listHoursRaw = listHours.Skip(index).ToList();
                        currentDate = dateEnd;
                        scheduleRaw = scheduleSalida;
                    }
                    else if (index == 0)
                        // solo pasa cuando son los mismos datos de fecha
                        listHoursRaw = listHours;
                    else
                        listHoursRaw = listHours.Take(index).ToList();

                    #region CoreExtraHours

                    // validar su horario habitual
                    var listIncludeschedule = listExtra.Hours.Intersect(scheduleRaw).ToList();

                    listHoursRaw = listHoursRaw.Except(scheduleRaw).ToList();
                    // validacion adicional
                    listHoursRaw = DateHelper.CalMinutesSchedules(listHoursRaw, scheduleRaw);

                    listHoursRaw = DateHelper.SettingHoursByExtraHours(listHoursRaw, listExtra.Hours, listExtra.Operator);

                    // esto se hace para validar los festivos y saber si estan permitidos en la plantillas
                    if (dataHolidays.Contains(currentDate))
                        validateDay = listExtra.Days.Contains("Holiday");
                    else
                        validateDay = listExtra.Days.Contains(currentDate.DayOfWeek.ToString());

                    // validar si el horario esta incluido en ese día
                    if (listDaysWork.Contains(currentDate.DayOfWeek.ToString()))
                        validateWork = true;
                    else
                        validateWork = false;

                    // esto ayuda a corregir un tema de horas
                    if (listHoursRaw.Any() && listHoursRaw.Count() > 2 && listHoursRaw.Last().Item2 == 0 && listHoursRaw.First().Item2 == 0)
                        listHoursRaw.Remove(listHoursRaw.Last());

                    //if (listHoursRaw.Any() && listHoursRaw.Count() >= 2 && listHoursRaw.FirstOrDefault().Item2 > 0 && listHoursRaw[1].Item2 == 0 && scheduleRaw.FirstOrDefault().Item2 == listHoursRaw.FirstOrDefault().Item2)
                    //    listHoursRaw.Remove(listHoursRaw.First());

                    // HORA EXTRA DIURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 10:00 PM LUNES - SABADO
                    if (listExtra.Type == ExtraHourType.HED && validateDay)
                    {
                        dataEmployeeRaw.HED += ((listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2));
                    }

                    // HORA EXTRA NOCTURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 10:01 PM Y 6:00 AM LUNES - SABADO
                    if (listExtra.Type == ExtraHourType.HEN && validateDay)
                    {
                        dataEmployeeRaw.HEN += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                    }

                    // RECARGO NOCTURNO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 10:01PM Y 6:00 AM LUNES - SABADO // ****
                    if (listExtra.Type == ExtraHourType.RN && validateDay && validateWork && listIncludeschedule.Any())
                    {
                        dataEmployeeRaw.RN += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                    }

                    // RECARGO NOCTURNO FESTIVO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 10:01PM Y 6:00 AM DOMINGOS Y FESTIVOS // ****
                    if (listExtra.Type == ExtraHourType.RNF && validateDay && listIncludeschedule.Any())
                    {
                        dataEmployeeRaw.RNF += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                    }

                    // HORA EXTRA DIRUNA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 10:00 PM DOMINGOS Y FESTIVOS
                    if (listExtra.Type == ExtraHourType.HEDF && validateDay)
                    {
                        dataEmployeeRaw.HEDF += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                    }

                    // HORA EXTRA NOCTURA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 10:01 PM Y 6:00 AM DOMINGOS Y FESTIVOS
                    if (listExtra.Type == ExtraHourType.HENF && validateDay)
                    {
                        dataEmployeeRaw.HENF += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                    }

                    // DOMINICAL CON COMPENSATORIO - SI EL DOMINGO ESTA INCLUIDO EN SU HORARIO HABITUAL, TODAS LAS HORAS DE ESE DOMINGO TIENEN ESTE RECARGO, DESPUES DEL TERCER DOMINGO SE CONVIERTE EN DSC EN HORARIO DE 6:00 AM HASTA 10:00PM
                    if (listExtra.Type == ExtraHourType.DCC && validateDay && validateWork && listIncludeschedule.Any())
                    {
                        dataEmployeeRaw.DCC += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                        dataEmployeeRaw.TotalSunday += 1;
                    }
                    // DESPUES DE PAGAR 3 DCC SEGUIDOS, TODAS LAS HORAS DEL 4 DOMINGO ES DSC EN HORARIO DE 6AM A 10PM
                    if (listExtra.Type == ExtraHourType.DSC && validateDay && dataEmployeeRaw.TotalSunday > 3)
                    {
                        dataEmployeeRaw.DCC += (listHoursRaw.Where(x => x.Item2 == 0).Count() * 60) + listHoursRaw.Sum(x => x.Item2);
                        dataEmployeeRaw.TotalSunday += 1;
                    }

                    #endregion
                }

            }
        }

        public static void SendMailPassword(UserIdentity user)
        {
            var mailsToSend = new List<Mail>();

            mailsToSend.Add(new Mail {
                Body = GetTemplateUserData("PasswordMail", user),
                Subject = "Recuperar Contrasena - Radiopolis",
                toAddress = user.Email,
                BCC = "analistarrhh@vibra.fm"
            });
            if (mailsToSend.Any())
                MailSender.Send(mailsToSend);

        }

        private static string GetTemplateUserData(string template, UserIdentity user)
        {
            string text;
            var fileStream = new FileStream(@"C:\Resources\MailTemplate\" + template + ".html", FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8)) text = streamReader.ReadToEnd();
            text = text.Replace("@Nombres", user.Name);
            text = text.Replace("@Contrasena", user.Contrasena);

            return text;
        }

    }
}