﻿using ExtraHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.Services
{
    public class WorkdayProcess
    {
        private readonly IEnumerable<DateTime> _holidays;
        private readonly IEnumerable<Horario> _schedules;
        public WorkdayProcess(IEnumerable<DateTime> holidays, IEnumerable<Horario> schedules)
        {
            _holidays = holidays;
            _schedules = schedules;
        }

        /// <summary>
        /// Procesa el cálculo del tiempo trabajado por empleado
        /// </summary>
        /// <param name="employee">Datos del empleado</param>
        /// <param name="workedDays">Lista de jornadas trabajadas y su correspondiente horario</param>
        /// <param name="employeeSchedules">Lista de horarios asignados al empleado en el tiempo</param>
        /// <returns></returns>
        public ExtraHourFile ProcessWordTimeByEmployee(EmployeeExtended employee, IEnumerable<Tuple<DateTime,DateTime, int>> workedDays)
        {
            var hoursFile = new ExtraHourFile(employee);
            
            foreach(var wd in workedDays)
            {
                var schedule = _schedules.FirstOrDefault(x => x.IDHorario == wd.Item3);
                ProcessHours(hoursFile, wd.Item1, wd.Item2, schedule);
            }
            hoursFile.AddExtended(employee);
            return hoursFile;
        }

        /// <summary>
        /// Ejecuta el cálculo de tiempo trabajado
        /// </summary>
        /// <param name="hoursFile">Objeto donde se acumula la info calculada</param>        
        /// <param name="startDate">Fecha y hora de ingreso</param>
        /// <param name="finishDate">Fecha y hora de salida</param>
        /// <param name="schedule">Horario que debe cumplir el empleado el día procesado</param>
        private void ProcessHours(ExtraHourFile hoursFile, DateTime startDate, DateTime finishDate, Horario schedule)
        {
            // Se evalúa minuto a minuto, desde la hora de ingreso hasta la hora de salida
            var sundays = new List<DateTime>();
            var current = startDate;
            while(current <= finishDate)
            {
                // Evalúa si el minuto analizado está en el horario, para saber si es extra o no.
                // Si no tiene horario o no tiene rango de horas para el día analizado se asume que está en horario extra

                var isScheduled = false;
                var scheduledWorkday = GetScheduledWorkday(schedule, current);
                if(scheduledWorkday != null && scheduledWorkday.Item1.HasValue && scheduledWorkday.Item2.HasValue)
                {
                    // Tiene horario, se busca si el minuto evaluado está dentro o fuera del horario
                    var scheduleStart = current.Date + scheduledWorkday.Item1;
                    // Si la hora de finalización es menor a la de inicio significa que es del día siguiente, sino es del mismo día
                    var scheduleEnd = scheduledWorkday.Item1 < scheduledWorkday.Item2 ? current.Date + scheduledWorkday.Item2 : current.Date.AddDays(1) + scheduledWorkday.Item2;
                    // Está en horario? sino es hora extra
                    isScheduled = (current >= scheduleStart && current <= scheduleEnd);
                }

                // TOCA SACAR ESTA CONDICION YA QUE NO ES POR QUE NO ESTE DENTRO DE SU HORARIO SIPLEMENTE ES "FESTIVO"
                if (_holidays.Contains(current.Date) || (current.Date.DayOfWeek == DayOfWeek.Sunday && !isScheduled))
                {
                    //// HEDF: HORA EXTRA DIURNA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 9:00 PM DOMINGOS Y FESTIVOS
                    //// HENF: HORA EXTRA NOCTURA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 10:01 PM Y 6:00 AM DOMINGOS Y FESTIVOS
                    if (current > current.Date + new TimeSpan(6, 0, 0) && current <= current.Date + new TimeSpan(21, 0, 0))
                        hoursFile.HEDF++;
                    else
                        hoursFile.HENF++;

                    current = current.AddMinutes(1);
                    continue;
                }

                if (!isScheduled)
                {
                    //// HED: HORA EXTRA DIURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 9:00 PM LUNES - SABADO
                    //// HEN: HORA EXTRA NOCTURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 9:01 PM Y 6:00 AM LUNES - SABADO
                    if (current > current.Date + new TimeSpan(6, 0, 0) && current <= current.Date + new TimeSpan(21, 0, 0))
                        hoursFile.HED++;
                    else
                        hoursFile.HEN++;
                }
                else
                {
                    // Recargos durante jornadas laborales
                    if (current <= current.Date + new TimeSpan(6, 0, 0) || current > current.Date + new TimeSpan(21, 0, 0))
                    {
                        //// RN: RECARGO NOCTURNO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 9:01PM Y 6:00 AM LUNES - SABADO // ****
                        //// RNF: RECARGO NOCTURNO FESTIVO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 9:01PM Y 6:00 AM DOMINGOS Y FESTIVOS // ****
                        if (_holidays.Contains(current.Date) || current.Date.DayOfWeek == DayOfWeek.Sunday)
                            hoursFile.RNF++;
                        else
                            hoursFile.RN++;
                    }
                    else
                    {
                        if(current.Date.DayOfWeek == DayOfWeek.Sunday)
                        {
                            //// DCC: DOMINICAL CON COMPENSATORIO - SI EL DOMINGO ESTA INCLUIDO EN SU HORARIO HABITUAL, TODAS LAS HORAS DE ESE DOMINGO TIENEN ESTE RECARGO, DESPUES DEL TERCER DOMINGO SE CONVIERTE EN DSC EN HORARIO DE 6:00 AM HASTA 10:00PM
                            //// DSC: DESPUES DE PAGAR 3 DCC SEGUIDOS, TODAS LAS HORAS DEL 4 DOMINGO ES DSC EN HORARIO DE 6AM A 10PM
                            if (!sundays.Contains(current.Date)) sundays.Add(current.Date);
                            if (sundays.Count <= 3) hoursFile.DCC++;
                            else hoursFile.DSC++;
                        }
                    }
                }                
                current = current.AddMinutes(1);
            }
        }

        private Tuple<TimeSpan?, TimeSpan?> GetScheduledWorkday(Horario schedule, DateTime current)
        {
            Tuple<TimeSpan?, TimeSpan?> result = null;
            if(schedule != null)
            {
                switch(current.DayOfWeek)
                {
                    // Aquí se asume que cuando se inicia un día y se finaliza en otro las horas que aparecen son las de inicio del día actual y fin del siguiente
                    // Por ejemplo, si inicia el lunes a las 22:00 y termina el martes a las 06:00, queda LunesEntrada=22:00 y LunesSalida=06:00
                    case DayOfWeek.Monday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.LunesEntrada, schedule.LunesSalida); break;
                    case DayOfWeek.Tuesday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.MartesEntrada, schedule.MartesSalida); break;
                    case DayOfWeek.Wednesday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.MiercolesEntrada, schedule.MiercolesSalida); break;
                    case DayOfWeek.Thursday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.JuevesEntrada, schedule.JuevesSalida); break;
                    case DayOfWeek.Friday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.ViernesEntrada, schedule.ViernesSalida); break;
                    case DayOfWeek.Saturday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.SabadoEntrada, schedule.SabadoSalida); break;
                    case DayOfWeek.Sunday: result = new Tuple<TimeSpan?, TimeSpan?>(schedule.DomingoEntrada, schedule.DomingoSalida); break;
                    default: break;
                }                        
            }
            return result;
        }

    }
}
