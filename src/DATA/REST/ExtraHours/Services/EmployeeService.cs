﻿using ExtraHours.DataBase;
using ExtraHours.Helpers;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ExtraHours.Services
{
    public class EmployeeService
    {
        public static Stream GetFileExcel(DateTime dateIni, DateTime dateEnd)
        {
            try
            {
                // datos listos para formar excel
                var data = EmployeeDB.GetAllEmployeesExtended();

                // proceso de llenado de excel esto es lo facil
                var headers = new List<object[]>();

                headers.Add(ExcelHelper.GetSpreadSheetEmployeeHeader());

                using (ExcelPackage pck = new ExcelPackage())
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Reporte Empleados");
                    pck.Workbook.Properties.Author = "HorasExtrasWWW-GPR";

                    var recordsDecoded = new List<object[]>();

                    foreach (var da in data)
                    {
                        var jefe = data.Where(x => x.IDEmpleado == da.IDJefe).FirstOrDefault();
                        recordsDecoded.Add(ExcelHelper.ConvertEmployeeFileToObject(da, $"{jefe?.Nombres} {jefe?.Apellidos}"));
                    }

                    ws.Cells["A1"].Value = "Reporte Empleados";
                    ws.Cells["A1"].Style.Font.Bold = true;


                    ws.Cells["A3"].LoadFromArrays(headers);
                    ws.Cells["A3:O6"].Style.Font.Bold = true;
                    ws.Cells["A3:O6"].AutoFitColumns();

                    if (recordsDecoded.Any())
                        ws.Cells["A4"].LoadFromArrays(recordsDecoded);

                    return new MemoryStream(pck.GetAsByteArray());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

    }
}