﻿using ExtraHours.DataBase;
using ExtraHours.Models;
using NLog;
using Novacode;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace ExtraHours.Services
{
    public static class UtilsService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static Stream GetFileDocVacation(int id, string filename)
        {
            try
            {
                // Debo traer toda la info de la platilla y ademas de los datos extras
                var vacationBase = ResquestDB.GetRequestVacationById(id);
                var userFull = EmployeeDB.GetEmployeesExtendedById(new List<int> { vacationBase.Item1 }).FirstOrDefault();
                var allVacation = VacationDB.GetAllByEmployee(vacationBase.Item1);
                List<string> dates = vacationBase.Item2.Dates.Split(new char[] { ',' }).ToList();
                var newAllVacation = new List<VacationPeriod>();

                var periodsCalculate = ResquestDB.GetDiscountPeriods(id);

                foreach (var item in allVacation)
                {
                    var pc = periodsCalculate.Where(x => x.Item1.Id == item.Id).Select(c => c.Item2).FirstOrDefault();

                    var newData = new VacationPeriod(item, pc + item.DiasTomados);
                    
                    newAllVacation.Add(newData);
                }

                using (DocX document = DocX.Load(@"C:\sites\DocVacation.docx"))
                {
                    // Replace text in this document.
                    document.ReplaceText("@DiaHoy", DateTime.Now.ToString("dd 'de' MMMM 'de' yyyy", CultureInfo.CreateSpecificCulture("es-ES")));
                    document.ReplaceText("@NombreCompleto", userFull.Nombres +' '+ userFull.Apellidos);
                    document.ReplaceText("@Nombre", userFull.Nombres);
                    document.ReplaceText("@Cargo", userFull.Cargo);
                    document.ReplaceText("@Empresa", userFull.Empresa);

                    var periodosNew = string.Empty;
                    foreach (var item in periodsCalculate.Where(x => x.Item2 > 0))
                    {

                        periodosNew += item.Item1.Periodo + " ("+ ConvertRangeDatePeriodo(userFull.FechaIngreso, Int32.Parse(item.Item1.Periodo.Split('-')[1])) + ") ";
                    }
                    document.ReplaceText("@PeriodoLista", periodosNew);
                    document.ReplaceText("@DiasDisfrutoNumero", (vacationBase.Item2.Disfrutados+ vacationBase.Item2.Pagos).ToString());
                    var periodos = string.Empty;

                    foreach (var vaca in newAllVacation.Where(x => x.DiasCalculate > 0 ))
                    {
                        periodos += $"Le recordamos que para el período del {ConvertRangeDatePeriodo(userFull.FechaIngreso, Int32.Parse(vaca.Periodo.Split('-')[1]))} tiene pendiente ({vaca.DiasCalculate}) días de vacaciones.";
                    }

                    document.ReplaceText("@Periodos", periodos);
                    document.ReplaceText("@DiasDisfruto", vacationBase.Item2.Disfrutados.ToString());
                    if (vacationBase.Item2.Pagos > 0)
                        document.ReplaceText("@DiasRemunerado", "Y tendrá ("+vacationBase.Item2.Pagos.ToString()+") días que serán pagos o remunerados.");
                    else
                        document.ReplaceText("@DiasRemunerado", "");

                    document.ReplaceText("@DiaInicio", dates.FirstOrDefault());
                    document.ReplaceText("@DiaFin", dates.LastOrDefault());
                    document.ReplaceText("@DiaReintegro", CalculateDayReturn(DateTime.Parse(dates.LastOrDefault())));

                    var name = @"C:\sites\Files\" + filename;
                    document.SaveAs(name);
                    FileStream stream = new FileStream(name, FileMode.Open);
                    return stream;

                } 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                logger.Error(ex.Message);
                throw;
            }
        }

        private static string ConvertRangeDatePeriodo(DateTime date, int periodo)
        {
            date.AddDays(-1);
            return date.Day + "/" + date.Month + "/" + (periodo - 1) + " - " + date.Day + "/" + date.Month + "/" + (periodo);
            //return string.Empty;
        }

        private static string CalculateDayReturn(DateTime date)
        {
            // se agrega un día con el hecho de saber que puede ser el posible fecha de ingreso
            date = date.AddDays(1);

            var holidays = UtilsDB.GetHolidays();

            while (true)
            {
                // si ese día es festivo o domingo agregue 1 día
                if (holidays.Contains(date) || date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                    date = date.AddDays(1);
                else
                    break;
            }

            return date.ToString("dd/MM/yyyy");
        }
    }
}