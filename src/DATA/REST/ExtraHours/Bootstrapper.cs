﻿using Nancy;
using Nancy.Authentication.Basic;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;
using NLog;
using System;
using System.Reflection;

namespace ExtraHours
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            pipelines.EnableBasicAuthentication(new BasicAuthenticationConfiguration(container.Resolve<IUserValidator>(),"MyRealm"));


            // Register the custom exceptions handler.
            pipelines.OnError += (ctx, err) => HandleExceptions(err, ctx); ;
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);

            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("app", "app"));
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            StaticConfiguration.DisableErrorTraces = false;
            //CORS Enable
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                                .WithHeader("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE")
                                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type, Authorization, X-Requested-With");

            });
        }

        private static Response HandleExceptions(Exception err, NancyContext ctx)
        {
            var result = new Response();

            result.ReasonPhrase = err.Message;

            if (err is NotImplementedException)
            {
                result.StatusCode = HttpStatusCode.NotImplemented;
            }
            else if (err is UnauthorizedAccessException)
            {
                result.StatusCode = HttpStatusCode.Unauthorized;
            }
            else if (err is ArgumentException)
            {
                result.StatusCode = HttpStatusCode.BadRequest;
            }
            else if (err is TargetInvocationException)
            {
                result.ReasonPhrase = err.InnerException.Message;
                result.StatusCode = HttpStatusCode.BadRequest;
            }
            else
            {
                // An unexpected exception occurred!
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            logger.Error("Error HandleExceptions : " + result.ReasonPhrase);

            return result;
        }
    }
}