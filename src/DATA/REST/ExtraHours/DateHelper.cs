﻿using ExtraHours.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours
{
    public class DateHelper
    {
        public static List<Tuple<int, int>> GetRangeHoursDates(DateTime fechaEntrada, DateTime fechaSalida, bool fixscheduler = false)
        {
            var retorno = new List<Tuple<int, int>>();

            int fixHour = 1;
            //esto toco hacerlo por que cuenta una hora mas cuando tiene minutos
            if (fechaSalida.Minute != 0)
            {
                fixHour = 1;
            }

            var listRam = Enumerable.Range(0, fixHour + fechaSalida.Subtract(fechaEntrada).Hours)
                  .Select(offset => fechaEntrada.AddHours(offset).Hour)
                  .ToList();

            for (int i = 0; i < listRam.Count(); i++)
            {
                if (i == 0)
                {
                    retorno.Add(Tuple.Create(listRam[i], fechaEntrada.Minute));
                }
                else if (i == listRam.Count() - 1)
                {
                    if (fixscheduler)
                    {
                        retorno.Add(Tuple.Create(listRam[i], 0));
                    }
                    retorno.Add(Tuple.Create(listRam[i], fechaSalida.Minute));
                }
                else
                {
                    retorno.Add(Tuple.Create(listRam[i], 0));
                }
            }
            return retorno;
        }

        public static List<Tuple<int, int>> GetScheduleByDay(DateTime date, Horario schedule)
        {
            var retorno = new List<Tuple<int, int>>();
            DateTime dtInit = new DateTime(2015, 01, 01);
            DateTime dtEnd = new DateTime(2015, 01, 02);

            switch (date.DayOfWeek.ToString())
            {
                case "Monday":
                    if (!schedule.LunesEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.LunesEntrada.Value;
                    dtEnd = dtEnd + schedule.LunesSalida.Value;
                    break;
                case "Tuesday":
                    if (!schedule.MartesEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.MartesEntrada.Value;
                    dtEnd = dtEnd + schedule.MartesSalida.Value;
                    break;
                case "Wednesday":
                    if (!schedule.MiercolesEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.MiercolesEntrada.Value;
                    dtEnd = dtEnd + schedule.MiercolesSalida.Value;
                    break;
                case "Thursday":
                    if (!schedule.JuevesEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.JuevesEntrada.Value;
                    dtEnd = dtEnd + schedule.JuevesSalida.Value;
                    break;
                case "Friday":
                    if (!schedule.ViernesEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.ViernesEntrada.Value;
                    dtEnd = dtEnd + schedule.ViernesSalida.Value;
                    break;
                case "Saturday":
                    if (!schedule.SabadoEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.SabadoEntrada.Value;
                    dtEnd = dtEnd + schedule.SabadoSalida.Value;
                    break;
                case "Sunday":
                    if (!schedule.DomingoEntrada.HasValue) return retorno;
                    dtInit = dtInit + schedule.DomingoEntrada.Value;
                    dtEnd = dtEnd + schedule.DomingoSalida.Value;
                    break;
                default:
                    break;
            }

            retorno = GetRangeHoursDates(dtInit, dtEnd, true);
            return retorno;
        }

        public static List<string> GetDaysWork(Horario schedule)
        {
            var retorno = new List<string>();
            if (schedule.LunesEntrada != null) retorno.Add("Monday");
            if (schedule.MartesEntrada != null) retorno.Add("Tuesday");
            if (schedule.MiercolesEntrada != null) retorno.Add("Wednesday");
            if (schedule.JuevesEntrada != null) retorno.Add("Thursday");
            if (schedule.ViernesEntrada != null) retorno.Add("Friday");
            if (schedule.SabadoEntrada != null) retorno.Add("Saturday");
            if (schedule.DomingoEntrada != null) retorno.Add("Sunday");
            return retorno;
        }

        public static List<Tuple<int, int>> SettingHoursByExtraHours(List<Tuple<int, int>> listHoursRaw, List<Tuple<int, int>> listHoursExtra, bool opera)
        {
            var lastHour = listHoursExtra.LastOrDefault();

            //if (!listHoursExtra.Any(x => x.Item1 == 0 && !listHoursRaw.Any(c => c.Item1 == 0))) return new List<Tuple<int, int>>();
            listHoursRaw = listHoursRaw.Where(f => listHoursExtra.Any(b => (f.Item2 > b.Item2 && f.Item1 == b.Item1) || f.Item1 == b.Item1)).ToList();
            //listHoursRaw = listHoursExtra.Intersect(listHoursRaw).ToList();

            var itemHour = listHoursRaw.Where(x => x.Item1 == lastHour.Item1).FirstOrDefault();

            if (itemHour != null)
            {
                listHoursRaw.Remove(itemHour);
                if (opera)
                    listHoursRaw.Add(lastHour);
            }
            return listHoursRaw;
        }

        public static List<Tuple<int, int>> CalMinutesSchedules(List<Tuple<int, int>> listHoursRaw, List<Tuple<int, int>> scheduleRaw)
        {
            var retorno = new List<Tuple<int, int>>();
            if (!listHoursRaw.Any()) return retorno;
            if (!scheduleRaw.Any()) return listHoursRaw;
            
            var raw = scheduleRaw.Last();

            if (raw.Item2 != 0 )
            {
                foreach (var item in listHoursRaw)
                {
                    if (item.Item2 >= 0 && item.Item1 == raw.Item1 + 1)
                        retorno.Add(Tuple.Create(raw.Item1, (60 - raw.Item2)));
                    retorno.Add(item);
                }
            }
            return retorno;
        }
    }
}