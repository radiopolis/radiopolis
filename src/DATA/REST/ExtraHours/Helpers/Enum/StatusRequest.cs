﻿namespace ExtraHours.Helpers.Enum
{
    public enum StatusRequest
    {
        Creado = 1,
        Aprobado = 2,
        Rechazado = 3,
        Autorizado = 4,
        Liquidado = 5
    };

}