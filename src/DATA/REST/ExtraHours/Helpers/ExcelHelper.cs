﻿using System;
using ExtraHours.Models;

namespace ExtraHours.Helpers
{
    public static class ExcelHelper
    {
        public static object[] GetSpreadSheetHeader()
        {
            return new object[] {
                                    "Empresa",
                                    "Area",
                                    "SubArea",
                                    "Cedula",
                                    "Nombres",
                                    "Apellidos",
                                    "Cargo",
                                    "Centro Costo",
                                    "SubCentro Costo",
                                    "HED",
                                    "HEN",
                                    "RN",
                                    "RNF",
                                    "HEDF",
                                    "HENF",
                                    "DCC",
                                    "DSC",
                                    "Total Domingos",
            };
        }

        public static object[] ConvertExtraHourFileToObject(ExtraHourFile record)
        {
            var hourTime = 1;
            return new object[]{
                                record.Empresa,
                                record.Area,
                                record.SubArea,
                                record.Cedula,
                                record.Nombres,
                                record.Apellidos,
                                record.Cargo,
                                record.CentroCosto,
                                record.SubCentroCosto,
                                (record.HED/hourTime),
                                (record.HEN/hourTime),
                                (record.RN/hourTime),
                                (record.RNF/hourTime),
                                (record.HEDF/hourTime),
                                (record.HENF/hourTime),
                                (record.DCC/hourTime),
                                (record.DSC/hourTime),
                                record.TotalSunday
            };
        }



        public static object[] GetSpreadSheetDetaillHeader()
        {
            return new object[] {
                                    "Cedula",
                                    "Nombres",
                                    "Horario",
                                    "FechaEntrada",
                                    "FechaSalida",
                                    "FechaAprobado",
                                    "Observaciones",
                                    "Estado"
            };
        }

        public static object[] ConvertDetaillExtraHourFileToObject(ExtraHourDetailExtended record, Tuple<string, string> extdata, string horario)
        {
            return new object[]{
                                extdata.Item1,
                                extdata.Item2,
                                horario,
                                record.FechaEntrada.ToString("dd/MM/yyyy HH:mm:ss"),
                                record.FechaSalida.ToString("dd/MM/yyyy HH:mm:ss"),
                                record?.FechaAprobado?.ToString("dd/MM/yyyy HH:mm:ss"),
                                record.Observaciones,
                                record.Estado
            };
        }

        public static object[] GetSpreadSheetEmployeeHeader()
        {
            return new object[] {
                                    "Empresa",
                                    "Area",
                                    "SubArea",
                                    "CentroDeCosto",
                                    "SubCentroDeCosto",
                                    "Cedula",
                                    "Nombres",
                                    "Apellidos",
                                    "FechaIngreso",
                                    "FechaTerminacion",
                                    "Telefono",
                                    "Celular",
                                    "Email",
                                    "CargoExtendido",
                                    "Jefe",
                                    "Estado",
                                    "Genero",
            };
        }

        public static object[] ConvertEmployeeFileToObject(EmployeeExtended record, string jefe)
        {
            return new object[]{
                                record.Empresa,
                                record.Area,
                                record.SubArea,
                                record.CentroCosto,
                                record.SubCentroCosto,
                                record.Cedula,
                                record.Nombres,
                                record.Apellidos,
                                record.FechaIngreso.ToShortDateString(),
                                record?.FechaTerminacionContrato?.ToShortDateString(),
                                record.Telefono,
                                record.Movil,
                                record.Email,
                                record.cargoExtendido,
                                jefe,
                                record.Estado,
                                record.Genero

            };
        }
    }
}