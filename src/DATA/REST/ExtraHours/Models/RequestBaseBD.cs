﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class RequestBaseBD
    {
        public int Id { get; set; }

        public int Type { get; set; }

        public string Description { get; set; }

        public int userRequest { get; set; }

        public string Note { get; set; }

        public DateTime DateRequest { get; set; }

        public int responsableRequest { get; set; }

        public string Status { get; set; }

        public string attachedFiles { get; set; }

        public string ExtendedData { get; set; }

    }

}