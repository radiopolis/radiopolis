﻿using Nancy.Security;
using System.Collections.Generic;

namespace ExtraHours.Models
{
    public class UserIdentity : IUserIdentity
    {
        public string UserName { get; set; }

        public string Name { get; set; }
        public int IDEmpleado { get; set; }
        public int IDJefe { get; set; }
        public int Acceso { get; set; }

        public int SubArea { get; set; }

        public string Contrasena { get; set; }

        public string Email { get; set; }
        public IEnumerable<string> Claims { get; set; }
        public UserIdentity()
        { }
        public UserIdentity(Employee data)
        {
            UserName = data.Cedula;
            Name = data.Nombres;
            IDEmpleado = data.IDEmpleado;
            IDJefe = data.IDJefe;
            Acceso = data.IDRol;
            SubArea = data.IDSubArea;
        }

        public UserIdentity(dynamic data)
        {
            UserName = data.Cedula;
            Name = data.Nombres + " " + data.Apellidos;
            IDEmpleado = data.IDEmpleado;
            IDJefe = data.IDJefe;
            Acceso = data.IDRol;
            Contrasena = data.Contrasena;
            Email = data.Email;
            SubArea = data.IDSubArea;
        }

    }
}