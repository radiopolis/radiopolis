﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class Company
    {
        public int IDEmpresa { get; set; }
        public string NIT { get; set; }
        public string Nombre { get; set; }
        
    }
}