﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class VacationPeriod
    {

        public VacationPeriod(dynamic data)
        {
            Id = data.Id;
            Periodo = Convert.ToString(data.Periodo-1)+" - "+ Convert.ToString(data.Periodo);
            DiasCalculate = (data.Dias - data.DiasTomados);
            Dias = data.Dias;
            DiasTomados = data.DiasTomados;
        }

        public VacationPeriod(VacationPeriod data, int diasTomados)
        {
            Id = data.Id;
            Periodo = data.Periodo;
            DiasCalculate = (data.Dias - diasTomados);
            Dias = data.Dias;
            DiasTomados = data.DiasTomados;
        }

        public VacationPeriod() { }

        public int Id { get; set; }

        public string Periodo { get; set; }

        public int DiasCalculate { get; set; } // dias pendientes menos dias tomados

        public int Dias { get; set; }

        public int DiasTomados { get; set; }
    }
}