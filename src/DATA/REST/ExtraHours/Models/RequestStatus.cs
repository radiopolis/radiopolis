﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class RequestStatus
    {
        public int RequestId { get; set; }

        public string NewStatus { get; set; }

        public string Note { get; set; }
    }
}