﻿using System;

namespace ExtraHours.Models
{
    public class RequestTrace
    {
        public RequestTrace()
        {
        }

        public RequestTrace(dynamic data)
        {
            Id = data.Id;
            IdSolicitud = data.IdSolicitud;
            Estado = data.Estado;
            Responsable = data.Responsable;
            Fecha = data.Fecha;
        }

        public int Id { get; set; }

        public int IdSolicitud { get; set; }

        public string Estado { get; set; }

        public string Responsable { get; set; }

        public DateTime Fecha { get; set; }
    }
}