﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class RequestVacation
    {
        public int Disfrutados { get; set; }

        public int Pagos { get; set; }

        //public VacationPeriod Periodo { get; set; }

        // 21/12/2016, 21/12/2016, 21/12/2016
        public string Dates { get; set; }

        public string Observaciones { get; set; }

    }
}