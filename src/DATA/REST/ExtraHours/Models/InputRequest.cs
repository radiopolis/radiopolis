﻿using System;

namespace ExtraHours.Models
{
    public class InputRequest
    {
        public Parameter parameter { get; set; }
    }

    public class Parameter
    {
        public DateTime InitialDate { get; set; }

        public DateTime FinalDate { get; set; }
    }
}