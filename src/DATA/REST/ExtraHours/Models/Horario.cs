﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class Horario
    {
        public Horario()
        { }

        public int? IDHorario{ get; set; }
        public string Descripcion { get; set; }
        public TimeSpan? LunesEntrada { get; set; }
        public TimeSpan? LunesSalida { get; set; }
        public TimeSpan? MartesEntrada { get; set; }
        public TimeSpan? MartesSalida { get; set; }
        public TimeSpan? MiercolesEntrada { get; set; }
        public TimeSpan? MiercolesSalida { get; set; }
        public TimeSpan? JuevesEntrada { get; set; }
        public TimeSpan? JuevesSalida { get; set; }
        public TimeSpan? ViernesEntrada { get; set; }
        public TimeSpan? ViernesSalida { get; set; }
        public TimeSpan? SabadoEntrada { get; set; }
        public TimeSpan? SabadoSalida { get; set; }
        public TimeSpan? DomingoEntrada { get; set; }
        public TimeSpan? DomingoSalida { get; set; }

        public Horario(dynamic data)
        {
            IDHorario = data.IDHorario;
            Descripcion = data.Descripcion;
            LunesEntrada = data.LunesEntrada;
            LunesSalida = data.LunesSalida;

            MartesEntrada = data.MartesEntrada;
            MartesSalida = data.MartesSalida;

            MiercolesEntrada = data.MiercolesEntrada;
            MiercolesSalida = data.MiercolesSalida;

            JuevesEntrada = data.JuevesEntrada;
            JuevesSalida = data.JuevesSalida;

            ViernesEntrada = data.ViernesEntrada;
            ViernesSalida = data.ViernesSalida;

            SabadoEntrada = data.SabadoEntrada;
            SabadoSalida = data.SabadoSalida;

            DomingoEntrada = data.DomingoEntrada;
            DomingoSalida = data.DomingoSalida;
        }
    }
}