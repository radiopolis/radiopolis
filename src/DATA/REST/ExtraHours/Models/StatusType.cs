﻿namespace ExtraHours.Models
{
    public enum StatusType
    {
        S, // Sin Aprobar
        A, // Aprobadas
        L  // Liquidadas
    }
}