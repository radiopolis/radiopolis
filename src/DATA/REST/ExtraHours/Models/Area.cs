﻿namespace ExtraHours.Models
{
    public class Area
    {
        public int IDArea{ get; set; }
        public string Nombre { get; set; }
       
    }

    public class SubArea
    {
        public int IDSubArea { get; set; }
        public string Nombre { get; set; }
    }
}