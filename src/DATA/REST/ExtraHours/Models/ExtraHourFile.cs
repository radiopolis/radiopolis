﻿namespace ExtraHours.Models
{
    public class ExtraHourFile
    {
        public ExtraHourFile(Employee employee)
        {
            Cedula = employee.Cedula;
            Nombres = employee.Nombres;
            Apellidos = employee.Apellidos;
        }

        public void AddExtended(EmployeeExtended data)
        {
            Cedula = data.Cedula;
            Nombres = data.Nombres;
            Apellidos = data.Apellidos;

            Cargo = data.Cargo;
            CentroCosto = data.CentroCosto;
            SubCentroCosto = data.SubCentroCosto;

            Empresa = data.Empresa;
            Area = data.Area;
            SubArea = data.SubArea;
        }

        public ExtraHourFile(int iDEmpleado)
        {
            IDEmpleado = iDEmpleado;
        }
        // VALORES DE APP
        public int IDEmpleado { get; set; }
        // ##

        public string Empresa { get; set; }

        public string Area { get; set; }

        public string SubArea { get; set; }

        public string Cedula { get; set; }

        public string Nombres { get; set; }

        public string Apellidos { get; set; }

        public string Cargo { get; set; }

        public string CentroCosto { get; set; }

        public string SubCentroCosto { get; set; }

        public int HED { get; set; }

        public int HEN { get; set; }

        public int RN { get; set; }

        public int RNF { get; set; }

        public int HEDF { get; set; }

        public int HENF { get; set; }

        public int DCC { get; set; }

        public int DSC { get; set; }

        public int TotalSunday { get; set; }
    }
}