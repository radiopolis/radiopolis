﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.Models
{
    public class CostCenter
    {
        public int IDCentroCosto { get; set; }
        public string IDSistema { get; set; }
        public string Nombre { get; set; }
    }

    public class SubCostCenter
    {
        public int IDSubCentroCosto { get; set; }
        public string IDSistema { get; set; }
        public string Nombre { get; set; }
    }
}