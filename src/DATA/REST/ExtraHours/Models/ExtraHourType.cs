﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.Models
{
    public enum ExtraHourType
    {
        HED,  // HORA EXTRA DIURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 10:00 PM LUNES - SABADO
        HEN,  // HORA EXTRA NOCTURNA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 10:01 PM Y 6:00 AM LUNES - SABADO
        RN,   // RECARGO NOCTURNO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 10:01PM Y 6:00 AM LUNES - SABADO // ****
        RNF,  // RECARGO NOCTURNO FESTIVO - QUE SU HORARIO HABITUAL ESTE COMPRENDIDO ENTRE 10:01PM Y 6:00 AM DOMINGOS Y FESTIVOS // ****
        HEDF, // HORA EXTRA DIURNA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 6:01 AM Y 10:00 PM DOMINGOS Y FESTIVOS
        HENF, // HORA EXTRA NOCTURNA FESTIVA - DESPUES DE CUMPLIR SU HORARIO LABORAL ENTRE 10:01 PM Y 6:00 AM DOMINGOS Y FESTIVOS
        DCC,  // DOMINICAL CON COMPENSATORIO - SI EL DOMINGO ESTA INCLUIDO EN SU HORARIO HABITUAL, TODAS LAS HORAS DE ESE DOMINGO TIENEN ESTE RECARGO, DESPUES DEL TERCER DOMINGO SE CONVIERTE EN DSC EN HORARIO DE 6:00 AM HASTA 10:00PM
        DSC   // DESPUES DE PAGAR 3 DCC SEGUIDOS, TODAS LAS HORAS DEL 4 DOMINGO ES DSC EN HORARIO DE 6AM A 10PM
    }

    public class ExtraHourData
    {
        public ExtraHourType Type { get; set; }

        public List<Tuple<int,int>> Hours { get; set; }

        public List<string> Days { get; set; }

        public bool InSchedule { get; set; }

        // este flag permite definir si agrego o no la hora limite
        public bool Operator { get; set; }

        public static List<ExtraHourData> Get()
        {
            var retorno = new List<ExtraHourData>();
            var daysRaw = new List<Tuple<int,string>>();
            List<string> temp = new List<string>();

            // HEAD
            daysRaw.Add(Tuple.Create(1, "Monday"));
            daysRaw.Add(Tuple.Create(2, "Tuesday"));
            daysRaw.Add(Tuple.Create(3, "Wednesday"));
            daysRaw.Add(Tuple.Create(4, "Thursday"));
            daysRaw.Add(Tuple.Create(5, "Friday"));
            daysRaw.Add(Tuple.Create(6, "Saturday"));
            daysRaw.Add(Tuple.Create(7, "Sunday"));
            daysRaw.Add(Tuple.Create(8, "Holiday"));

            // HED
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.HED,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 6, 1, 0), new DateTime(2015, 01, 01, 22, 1, 0)),
                Days = daysRaw.Where(x=> x.Item1 >= 1 && x.Item1 <= 6).Select(x=> x.Item2).ToList(),
                InSchedule = false,
                Operator = true
            });

            // HEN
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.HEN,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 22, 1, 0), new DateTime(2015, 01, 02, 6, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 1 && x.Item1 <= 6).Select(x => x.Item2).ToList(),
                InSchedule = false,
                Operator = false
            });

            // RN
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.RN,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 22, 1, 0), new DateTime(2015, 01, 02, 6, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 1 && x.Item1 <= 6).Select(x => x.Item2).ToList(),
                InSchedule = true,
                Operator = false
            });

            // RNF
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.RNF,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 22, 1, 0), new DateTime(2015, 01, 02, 6, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 7 && x.Item1 <= 8).Select(x => x.Item2).ToList(),
                InSchedule = true,
                Operator = false
            });

            // HEDF
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.HEDF,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 6, 1, 0), new DateTime(2015, 01, 01, 22, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 7 && x.Item1 <= 8).Select(x => x.Item2).ToList(),
                InSchedule = false,
                Operator = true
            });

            // HENF
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.HENF,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 22, 1, 0), new DateTime(2015, 01, 02, 6, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 7 && x.Item1 <= 8).Select(x => x.Item2).ToList(),
                InSchedule = false,
                Operator = false
            });

            // DCC
            retorno.Add(new ExtraHourData
            {
                Type = ExtraHourType.DCC,
                Hours = DateHelper.GetRangeHoursDates(new DateTime(2015, 01, 01, 6, 0, 0), new DateTime(2015, 01, 01, 22, 0, 0)),
                Days = daysRaw.Where(x => x.Item1 >= 7 && x.Item1 <= 8).Select(x => x.Item2).ToList(),
                InSchedule = true,
                Operator = true
            });

            return retorno;
        }
    }
}