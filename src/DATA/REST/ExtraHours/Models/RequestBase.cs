﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.Models
{
    public class RequestBase
    {
        public RequestBase(dynamic data, Employee employee, Employee responsable)
        {
            Id = data.Id;
            Type = data.Type;
            Description = data.Description;
            userRequest = new UserRequest { Id = employee.IDEmpleado, Identification = Convert.ToInt32(employee.Cedula), Name = employee.ToString() };
            Note = data.Note;
            DateRequest = data.DateRequest;
            responsableRequest = new UserRequest { Id = responsable.IDEmpleado, Identification = Convert.ToInt32(responsable.Cedula), Name = responsable.ToString() };
            Status = data.Status;
            foreach (var item in data.attachedFiles.Split(',')) attachedFiles.Add(new attachedFiles { fileName = item });
            ExtendedData = data.ExtendedData;
        }

        public int Id { get; set; }

        public int Type { get; set; }

        public string Description { get; set; }

        public UserRequest userRequest { get; set; }

        public string Note { get; set; }

        public DateTime DateRequest { get; set; }

        public UserRequest responsableRequest { get; set; }

        public string Status { get; set; }

        public List<attachedFiles> attachedFiles { get; set; } = new List<attachedFiles>();

        public string ExtendedData { get; set; }

    }

    public class attachedFiles
    {
        public string fileName { get; set; }
    }

    public class UserRequest
    {
        public int Id { get; set; }

        public int Identification { get; set; }

        public string Name { get; set; }
    }
}