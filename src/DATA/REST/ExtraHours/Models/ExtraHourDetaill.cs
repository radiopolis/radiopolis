﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.Models
{
    public class ExtraHourDetaill
    {
        public ExtraHourDetaill()
        { }

        public ExtraHourDetaill(ExtraHourDetaill data)
        {
            Estado = data.Estado;
            IDReporteHE = data.IDReporteHE;
            Observaciones = data.Observaciones;
        }

        public int? IDHoraExtraDetalle { get; set; }
        public int IDReporteHE { get; set; }
        public int IDHorario { get; set; }
        public List<DateTime> fechas { get; set; }
        public DateTime horaInicial { get; set; }
        public DateTime horaFinal { get; set; }
        public string Observaciones { get; set; }
        public string Estado { get; set; }
    }

    public class ExtraHourDetailExtended : ExtraHourDetaill
    {
        public ExtraHourDetailExtended(ExtraHourDetaill extra, DateTime item, int userId)
            : base(extra)
        {
            FechaEntrada = new DateTime(item.Year, item.Month, item.Day, extra.horaInicial.Hour, extra.horaInicial.Minute, 0);

            DateTime fechaSalidaRaw = new DateTime(item.Year, item.Month, item.Day, extra.horaFinal.Hour, extra.horaFinal.Minute, 0);

            if (extra.horaInicial.Hour > extra.horaFinal.Hour)fechaSalidaRaw = fechaSalidaRaw.AddDays(1);

            FechaSalida = fechaSalidaRaw;

            IDEmpleado = userId;

            IDHorario = extra.IDHorario;

            FechaCreado = DateTime.Now;

            Estado = "S";
        }

        public ExtraHourDetailExtended()
        {
        }
        public int IDEmpleado { get; set; }
        public DateTime FechaCreado { get; set; }
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaSalida { get; set; }
        public DateTime? FechaAprobado { get; set; }
        public string Horario { get; set; }

    }

}