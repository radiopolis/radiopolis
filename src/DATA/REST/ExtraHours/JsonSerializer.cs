﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace ExtraHours
{
    public class JsonSerializer
    {
        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ContractResolver = new LowercaseContractResolver()
        };

        public static string SerializeObject(object o)
        {
            return JsonConvert.SerializeObject(o, Formatting.Indented, Settings);
        }

        public class LowercaseContractResolver : DefaultContractResolver
        {
            protected override string ResolvePropertyName(string propertyName)
            {
                return FirstCharacterToLower(propertyName);
            }
            public static string FirstCharacterToLower(string str)
            {
                if (string.IsNullOrEmpty(str) || char.IsLower(str, 0))
                    return str;

                return char.ToLowerInvariant(str[0]) + str.Substring(1);
            }
        }
    }

}