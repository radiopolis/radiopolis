﻿using Simple.Data;
using ExtraHours.Models;
using System.Collections.Generic;

namespace ExtraHours.DataBase
{
    public class AreaDB
    {
        public static dynamic CreateArea(Area area)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Empleados.Insert(area);
        }
        public static List<Area> GetAllAreas()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Areas.All();
        }

        public static List<SubArea> GetAllSubAreas()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.SubAreas.All();
        }
    }
}