﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simple.Data;
using ExtraHours.Models;

namespace ExtraHours.DataBase
{
    public class ExtraHourDB
    {
        public static void CreateExtraHourDetaill(ExtraHourDetaill extraHourDetaill, int userId)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            var datasql = new List<ExtraHourDetailExtended>();

            foreach (var item in extraHourDetaill.fechas) datasql.Add(new ExtraHourDetailExtended(extraHourDetaill, item, userId));

            db.HoraExtraDetalle.Insert(datasql);
        }

        public static List<ExtraHour> GetExtraHourByDate(int month)
        {
            DateTime dateInit = new DateTime(DateTime.Now.Year, month, 1,0,0,0);
            DateTime dateEnd = new DateTime(DateTime.Now.Year, 12, DateTime.DaysInMonth(DateTime.Now.Year,month),23,59,59);

            var db = Database.OpenNamedConnection("ExtraHours");

            return db.HorasExtra.All()
                                .Where(db.HorasExtra.Fecha >= dateInit && db.HorasExtra.Fecha <= dateEnd && db.HorasExtra.Estado == "S");
        }

        public static List<ExtraHourDetailExtended> GetExtraHourDetaillByIdReport(int idReport, int user)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            var data= db.HoraExtraDetalle.All()
                .Select(db.HoraExtraDetalle.AllColumns(),
                        db.Horarios.Descripcion.As("Horario"))
                .Join(db.Horarios).On(db.Horarios.IDHorario == db.HoraExtraDetalle.IDHorario)
                .Where(db.HoraExtraDetalle.IDReporteHE == idReport && db.HoraExtraDetalle.IDEmpleado == user);

            return data;
        }

        public static List<ExtraHourDetailExtended> GetExtraHourDetaillByIdReport(List<int> ids)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.HoraExtraDetalle.All()
                .Where(db.HoraExtraDetalle.IDReporteHE == ids);
        }

        public static List<ExtraHourDetailExtended> GetExtraHourDetaillByDates(DateTime dateIni, DateTime dateEnd)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.HoraExtraDetalle.All()
                                      .Where(db.HoraExtraDetalle.Estado == "A" && db.HoraExtraDetalle.FechaAprobado >= dateIni && db.HoraExtraDetalle.FechaAprobado <= dateEnd)
                                      .OrderByFechaAprobado();
        }

        public static List<ExtraHourDetailExtended> GetAllExtraHourDetaillByDates(DateTime dateIni, DateTime dateEnd)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.HoraExtraDetalle.All()
                                      .Where(db.HoraExtraDetalle.FechaAprobado >= dateIni && db.HoraExtraDetalle.FechaAprobado <= dateEnd)
                                      .OrderByFechaAprobado();
        }

        public static List<dynamic> GetEmployeeExtraHours(int _employeeId, string _filter)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.sp_GetExtraHoursByEmployee(_employeeId, _filter); ;
        }

        public static List<dynamic> GetEmployeesByApprover(int idApprover)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            //TODO: ARREGLAR
            return db.PlantillasPorEmpleado.All()
                    .Join(db.Empleados).On(db.PlantillasPorEmpleado.IDEmpleado == db.Empleados.IDEmpleado)
                    .Where(db.Empleados.IDJefe == idApprover)
                    .Select(db.Empleados.IDJefe.As("IdPadre"), db.Empleados.IDEmpleado.As("IdHijo"), db.Empleados.Cedula, db.Empleados.Nombres, db.Empleados.Apellidos,
                             db.Empleados.Genero/*, db.Empleados.IDHorario*/, db.PlantillasPorEmpleado.Aprobadas, db.PlantillasPorEmpleado.SinAprobar,
                             db.PlantillasPorEmpleado.Liquidadas, db.PlantillasPorEmpleado.Rechazadas, db.PlantillasPorEmpleado.Total);
        }

        public static dynamic GetAllExtraHoursByEMployeeID(int employeeId)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.sp_GetExtraHoursByEmployee(employeeId); ;
        }

        public static void UpdateExtraHour(ExtraHour extraHour)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.HorasExtra.UpdateByIDReporteHE(extraHour);
        }

        public static void RemoveExtraHourDetail(int idExtraHourDetail)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.HoraExtraDetalle.DeleteByIDHoraExtraDetalle(idExtraHourDetail);
        }

        public static void UpdateExtraHourDetail(ExtraHourDetailExtended detail)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            DateTime? dateFix;
            // esto hay que hacerlo ya que solo en estado aprobado "A" debe asignarle "FechaCreado" si no null
            if (detail.Estado == "A")
                dateFix = DateTime.Now;
            else
                dateFix = null;
 
            db.HoraExtraDetalle.UpdateByIDHoraExtraDetalle(FechaAprobado: dateFix, Estado : detail.Estado, IDHoraExtraDetalle : detail.IDHoraExtraDetalle);
        }

        public static void UpdateStatusExtraHourDetail(int id, string status)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            db.HoraExtraDetalle.UpdateByIDHoraExtraDetalle(Estado: status, IDHoraExtraDetalle: id);
        }

        public static void DeleteSchedule(int _idReporteHE)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.HorasExtra.Delete(IDReporteHE: _idReporteHE);
        } 
    }
}