﻿using System.Collections.Generic;
using Simple.Data;
using ExtraHours.Models;

namespace ExtraHours.DataBase
{
    public class HorarioDB
    {
        public static void CreateHorario(Horario horario)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.Horarios.Insert(horario);
        }

        public static List<Horario> GetAllHorarios()
        {
            var retorno = new List<Horario>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var horarios = db.Horarios.All();

            foreach (var item in horarios) retorno.Add(new Horario(item));

            return retorno;
        }
        
        public static List<Horario> GetScheduleByEmployee(int idEmployee)
        {
            var retorno = new List<Horario>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Horarios.All()
                            .Join(db.EmpleadoHorarios).On(db.EmpleadoHorarios.IDHorario == db.Horarios.IDHorario)
                            .Where(db.EmpleadoHorarios.IDEmpleado == idEmployee);

            foreach(var item in data) retorno.Add(new Horario(item));

            return retorno;
        }

        public static void UpdateHorario(Horario horario)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.Horarios.UpdateByIDHorario(horario);
        }

        public static void DeleteSchedule(int idHorario)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            db.Horarios.Delete(IDHorario: idHorario);
        }
    }
}