﻿using ExtraHours.Models;
using Simple.Data;
using System.Linq;

namespace ExtraHours.DataBase
{
    public class UserIdentityDB
    {
        public static UserIdentity GetUserByCredentials(string userName, string password )
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                         .Where(db.Empleados.Cedula == userName && db.Empleados.Contrasena == password && db.Empleados.Estado == true).FirstOrDefault();

            return data != null ? new UserIdentity(new Employee(data)) : null ;
        }
    }
}