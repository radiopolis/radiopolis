﻿using System.Collections.Generic;
using System.Linq;
using Simple.Data;
using ExtraHours.Models;
using System;
using Nancy.Extensions;

namespace ExtraHours.DataBase
{
    public class EmployeeDB
    {
        public static dynamic CreateEmployee(Employee employee)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Empleados.Insert(employee);
        }

        public static List<Office> GetAllOffice()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Cargos.All();
        }

        public static List<Company> GetAllCompanies()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Empresas.All();
        }

        public static Employee GetEmployeeByIdentification(long identification)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.Clientes.All()
                .Where(db.empleados.Cedula == identification).FirstOrDefault();
        }

        public static IEnumerable<Employee> GetAllEmployees()
        {
            var retorno = new List<Employee>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno.OrderBy(x=> x.Apellidos);
        }

        public static Employee GetEmployeeByIdentificationSchedule(int identification)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            int count = 0;

            var emple = new Employee(db.Empleados.All()
                .Where(db.Empleados.Cedula == identification).FirstOrDefault());

            var horario = db.EmpleadoHorarios.All()
                            .Where(db.EmpleadoHorarios.IDEmpleado == emple.IDEmpleado);

            emple.IDHorario = new int[horario.Count()];
            foreach (var item in horario)
            {
                emple.IDHorario[count] = item.IDHorario;
                count++;
            }


            return emple;
        }

        public static List<Employee> GetEmployeesById(List<int> ids)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                .Where(db.Empleados.IDEmpleado == ids).ToList();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }

        public static List<Employee> GetEmployeesBySubArea(int subArea)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                .Where(db.Empleados.IDSubArea == subArea).ToList();

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }

        public static Employee GetEmployeeById(int id)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Empleados.All()
                .Where(db.Empleados.IDEmpleado == id).FirstOrDefault();

            return new Employee(data);
        }

        public static dynamic UpdateEmployee(Employee employee)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            //toca actualizar los horarios tambien
            // eliminamos los horarios y volvemos a insertar
            db.EmpleadoHorarios.DeleteByIDEmpleado(employee.IDEmpleado);

            foreach (var item in employee.IDHorario) db.EmpleadoHorarios.Insert(IDEmpleado: employee.IDEmpleado, IDHorario: item);

            return db.Empleados.UpdateByIDEmpleado(employee);
        }

        public static dynamic UpdatePassword(UserIdentity data)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            return db.Empleados.UpdateByIDEmpleado(IDEmpleado: data.IDEmpleado, Contrasena: data.Contrasena);
        }

        public static UserIdentity GetUserData(string username)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            var dataRaw = db.Empleados.All()
               .Where(db.Empleados.Cedula == username).FirstOrDefault();

            if (dataRaw == null) return null;

            return new UserIdentity(dataRaw);
        }

        public static List<Employee> GetEmployeesByScheduleID(int scheduleID)
        {
            var retorno = new List<Employee>();

            var db = Database.OpenNamedConnection("ExtraHours");
            var data = db.Empleados.All()
                .Join(db.EmpleadoHorarios).On(db.EmpleadoHorarios.IDEmpleado == db.Empleados.IDEmpleado)
                .Where(db.EmpleadoHorarios.IDHorario == scheduleID);

            foreach (var item in data) retorno.Add(new Employee(item));

            return retorno;
        }

        public static List<EmployeeExtended> GetAllEmployeesExtended()
        {
            var retorno = new List<EmployeeExtended>();

            var db = Database.OpenNamedConnection("ExtraHours");
            var data = db.Empleados.All()
                .Select(db.Empleados.AllColumns(),
                        db.CentroCosto.Nombre.As("CentroCosto"),
                        db.SubCentroCosto.Nombre.As("SubCentroCosto"),
                        db.Cargos.Cargo,
                        db.Empresas.Nombre.As("Empresa"),
                        db.Areas.Nombre.As("Area"),
                        db.SubAreas.Nombre.As("SubArea"))
                .Join(db.Cargos).On(db.Cargos.IDCargo == db.Empleados.IDCargo)
                .Join(db.SubCentroCosto).On(db.SubCentroCosto.IDSubCentroCosto == db.Empleados.IDSubCentro)
                .Join(db.CentroCosto).On(db.CentroCosto.IDCentroCosto == db.SubCentroCosto.IDCentroCosto)
                .Join(db.Empresas).On(db.Empresas.IDEmpresa == db.Empleados.IDEmpresa)
                .Join(db.Areas).On(db.Areas.IDArea == db.Empleados.IDArea)
                .Join(db.SubAreas).On(db.SubAreas.IDSubArea == db.Empleados.IDSubArea);

            foreach (var item in data) retorno.Add(new EmployeeExtended(item));

            return retorno.DistinctBy(p => p.IDEmpleado).OrderBy(x=> x.Apellidos).ToList();
        }

        public static List<EmployeeExtended> GetEmployeesExtendedById(List<int> employees)
        {
            if (!employees.Any()) return new List<EmployeeExtended>();
            var retorno = new List<EmployeeExtended>();

            var db = Database.OpenNamedConnection("ExtraHours");
            var data = db.Empleados.All()
                .Select(db.Empleados.AllColumns(),
                        db.CentroCosto.Nombre.As("CentroCosto"),
                        db.SubCentroCosto.Nombre.As("SubCentroCosto"),
                        db.Cargos.Cargo,
                        db.Empresas.Nombre.As("Empresa"),
                        db.Areas.Nombre.As("Area"),
                        db.SubAreas.Nombre.As("SubArea"))
                .Join(db.Cargos).On(db.Cargos.IDCargo == db.Empleados.IDCargo)
                .Join(db.SubCentroCosto).On(db.SubCentroCosto.IDSubCentroCosto == db.Empleados.IDSubCentro)
                .Join(db.CentroCosto).On(db.CentroCosto.IDCentroCosto == db.SubCentroCosto.IDCentroCosto)
                .Join(db.Empresas).On(db.Empresas.IDEmpresa == db.Empleados.IDEmpresa)
                .Join(db.Areas).On(db.Areas.IDArea == db.Empleados.IDArea)
                .Join(db.SubAreas).On(db.SubAreas.IDSubArea == db.Empleados.IDSubArea)
                .Where(db.Empleados.IDEmpleado == employees);

            foreach (var item in data) retorno.Add(new EmployeeExtended(item));

            return retorno.DistinctBy(p => p.IDEmpleado).ToList();
        }

    }
}