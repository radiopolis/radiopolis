﻿using ExtraHours.Models;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.DataBase
{
    public class ContractDB
    {
        public static dynamic CreateContract(Contract contract)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.TipoContratos.Insert(contract);
        }
        public static List<Contract> GetAllContracts()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.TipoContratos.All();
        }

    }
}