﻿using ExtraHours.Models;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtraHours.DataBase
{
    public class CostCenterDB
    {
        public static dynamic CreateCostCenter(CostCenter costCenter)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.CentroCosto.Insert(costCenter);
        }

        public static dynamic CreateSubCostCenter(int costCenterId, SubCostCenter costCenter)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            return db.SubCentroCosto.Insert(costCenter);
        }

        public static List<CostCenter> GetAllCostCenter()
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.CentroCosto.All();
        }

        public static List<SubCostCenter> GetAllSubCostCenter(int costCenterId)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            return db.SubCentroCosto.All()
                        .Select(db.SubCentroCosto.IDSubCentroCosto, db.SubCentroCosto.IDSistema, db.SubCentroCosto.Nombre)
                        .Where(db.SubCentroCosto.IDCentroCosto == costCenterId);
        }
    }
}