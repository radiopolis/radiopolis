﻿using Simple.Data;
using System;
using System.Collections.Generic;

namespace ExtraHours.DataBase
{
    public class UtilsDB
    {
        public static List<DateTime> GetHolidays()
        {
            DateTime dateInit = new DateTime(DateTime.Now.Year, 1, 1);
            DateTime dateEnd = new DateTime(DateTime.Now.Year, 12, 31);
            var retorno = new List<DateTime>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Festivos.All()
                                .Where(db.Festivos.Fecha >= dateInit && db.Festivos.Fecha <= dateEnd)
                                .Select(db.Festivos.Fecha).ToList();

            foreach (var item in data) retorno.Add((DateTime)item.Fecha);

            return retorno;
        }

        public static List<DateTime> GetHolidaysByDates(DateTime dateIni, DateTime dateEnd)
        {
            var retorno = new List<DateTime>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Festivos.All()
                                .Where(db.Festivos.Fecha >= dateIni && db.Festivos.Fecha <= dateEnd)
                                .Select(db.Festivos.Fecha).ToList();

            foreach (var item in data) retorno.Add((DateTime)item.Fecha);

            return retorno;
        }
    }
}