﻿using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using ExtraHours.Models;
using ExtraHours.Helpers.Enum;
using Nancy.Json;
using Radiopolis.Core.Utils;
using Radiopolis.Core.DataContracts;

namespace ExtraHours.DataBase
{
    public class ResquestDB
    {
        public static List<VacationPeriod> GetPeriods(int idEmployee)
        {
            var retorno = new List<VacationPeriod>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Vacaciones.All()
                                .Where(db.Vacaciones.IDEmpleado == idEmployee).ToList();

            foreach (var item in data) retorno.Add(new VacationPeriod(item));

            return retorno.Where(x => x.DiasCalculate > 0).ToList();

        }

        public static Tuple<int, RequestVacation> GetRequestVacationById(int id)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var data = db.Solicitudes.All()
                                .Where(db.Solicitudes.Id == id && db.Solicitudes.Type == 1).FirstOrDefault();

            return Tuple.Create((int)data.userRequest, serializer.Deserialize<RequestVacation>(data.ExtendedData));
        }

        public static void RemoveRequest(int id)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            db.Solicitudes.DeleteById(id);
        }

        public static void CreateVacation(RequestVacation data, UserIdentity user)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var JSON = serializer.Serialize(data);

            var request = new RequestBaseBD {
                Type = (int)TypeRequest.Vacations,
                Description = data.Observaciones,
                userRequest = user.IDEmpleado,
                Note = string.Empty,
                DateRequest = DateTime.Now,
                responsableRequest = user.IDJefe,
                Status  = StatusRequest.Creado.ToString(),
                attachedFiles = string.Empty,
                ExtendedData = JSON 
            };

            var insert = db.Solicitudes.Insert(request);

            CreateTrace(new RequestTrace { Estado = StatusRequest.Creado.ToString(), Fecha = DateTime.Now, Responsable = user.Name, IdSolicitud = insert.Id });
        }

        public static void UpdateVacation(int Id, RequestVacation data, UserIdentity user)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var JSON = serializer.Serialize(data);

            var request = new RequestBaseBD
            {
                Id = Id,
                Type = (int)TypeRequest.Vacations,
                Description = data.Observaciones,
                userRequest = user.IDEmpleado,
                Note = string.Empty,
                DateRequest = DateTime.Now,
                responsableRequest = user.IDJefe,
                Status = StatusRequest.Creado.ToString(),
                attachedFiles = string.Empty,
                ExtendedData = JSON
            };

            db.Solicitudes.UpdateById(request);

            CreateTrace(new RequestTrace { Estado = StatusRequest.Creado.ToString(), Fecha = DateTime.Now, Responsable = user.Name, IdSolicitud = Id});
        }

        public static void CreateTrace(RequestTrace data)
        {
            var db = Database.OpenNamedConnection("ExtraHours");
            db.VacacionesTraza.Insert(data);
        }

        public static void UpdateStatusRequest(List<RequestStatus> requests, UserIdentity user)
        {
            var db = Database.OpenNamedConnection("ExtraHours");

            foreach (var item in requests)
            {
                // si esta liquidado descuento los días de las vacaciones
                if (item.NewStatus == StatusRequest.Liquidado.ToString()) UpdateDaysVacation(item.RequestId);
                // Debe enviar solo notificacion a el area RH cuando el empleado lo aprueba
                if (item.NewStatus == StatusRequest.Aprobado.ToString()) SendNotification(item.RequestId);
                db.Solicitudes.UpdateById(Id: item.RequestId, Status: item.NewStatus, Note: item.Note, responsableRequest: user.IDEmpleado);

                CreateTrace(new RequestTrace { Estado = item.NewStatus, Fecha = DateTime.Now, Responsable = user.Name, IdSolicitud = item.RequestId });
            }
        }

        private static void SendNotification(int requestId)
        {
            var mails = new List<Mail>();
            // se traen solo los empleados de un subarea en este caso -- Recursos Humanos
            var employees = EmployeeDB.GetEmployeesBySubArea(13);

            var request = GetRequestVacationById(requestId);

            var userFull = EmployeeDB.GetEmployeesExtendedById(new List<int> { request.Item1 }).FirstOrDefault();

            foreach (var emp in employees)
            {
                mails.Add(new Mail
                {
                    toAddress = emp.Email,
                    Subject = "Se aprobo Vacaciones para el Colaborador - " + userFull.ToString(),
                    Body = $"Datos :<br> Días Disfrutados: {request.Item2.Disfrutados}<br> Días Remunerados: {request.Item2.Pagos}"
                });
            }

            if (mails.Any())
                MailSender.Send(mails);
        }

        private static void UpdateDaysVacation(int requestId)
        {
            var newVacations = GetDiscountPeriods(requestId);

            var db = Database.OpenNamedConnection("ExtraHours");

            foreach (var item in newVacations.Where(x=> x.Item2 > 0)) db.Vacaciones.UpdateById(Id: item.Item1.Id, DiasTomados: (item.Item1.DiasTomados + item.Item2));
        }

        public static List<Tuple<VacationPeriod, int>> GetDiscountPeriods(int requestId)
        {
            var vacationBase = GetRequestVacationById(requestId);
            var allVacation = VacationDB.GetAllByEmployee(vacationBase.Item1);
            var daysTotal = vacationBase.Item2.Disfrutados + vacationBase.Item2.Pagos;

            var newVacations = new List<Tuple<VacationPeriod, int>>();

            foreach (var item in allVacation.OrderBy(c => c.Periodo.Split('-')[0]))
            {
                daysTotal -= item.DiasCalculate;

                if (daysTotal >= 0) newVacations.Add(Tuple.Create(item, item.DiasCalculate));
                else newVacations.Add(Tuple.Create(item, (daysTotal + item.DiasCalculate) < 0 ? 0 : (daysTotal + item.DiasCalculate)));
            }
            return newVacations;
        }

        public static List<RequestTrace> GetTraceRequestById(int id)
        {
            var retorno = new List<RequestTrace>();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.VacacionesTraza.All()
                                .Where(db.VacacionesTraza.Id_Solicitud == id).ToList();

            foreach (var item in data) retorno.Add(new RequestTrace(item));

            return retorno.OrderBy(x=> x.Fecha).ToList();
        }

        public static List<RequestBase> GetRequestByTypeEmployee(int type, UserIdentity user)
        {
            var retorno = new List<RequestBase>();
            var employee = EmployeeDB.GetEmployeeById(user.IDEmpleado);
            var boss = EmployeeDB.GetEmployeeById(user.IDJefe);

            if (employee == null || boss == null) return retorno;

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Solicitudes.All()
                                .Where(db.Solicitudes.userRequest == user.IDEmpleado && db.Solicitudes.Type == type).ToList();

            foreach (var item in data) retorno.Add(new RequestBase(item, employee, boss));

            return retorno;
        }

        public static List<RequestBase> GetRequestByTypeBoss(int type, UserIdentity bossRaw, string Status)
        {
            var retorno = new List<RequestBase>();
            var boss = EmployeeDB.GetEmployeeById(bossRaw.IDEmpleado);

            if (boss == null) return retorno;

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Solicitudes.All()
                                .Where(db.Solicitudes.responsableRequest == boss.IDEmpleado && db.Solicitudes.Type == type && db.Solicitudes.Status == Status).ToList();

            foreach (var item in data) retorno.Add(new RequestBase(item, EmployeeDB.GetEmployeeById((int)item.userRequest), boss));

            return retorno;
        }

        public static List<RequestBase> GetRequestByAreaTypeStatus(int area, int type, string status ,UserIdentity userIdentify)
        {
            var retorno = new List<RequestBase>();
            //var boss = EmployeeDB.GetEmployeeById(bossRaw.IDEmpleado);

            var employeesByArea = EmployeeDB.GetEmployeesBySubArea(area);

            // validar que el usuario actual sea del area, si no no muestra nada
            if (!employeesByArea.Any(x => x.IDEmpleado == userIdentify.IDEmpleado)) return retorno;

            var listStatus = status.Split(',').ToList();

            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Solicitudes.All()
                                .Where(db.Solicitudes.Type == type && db.Solicitudes.Status == listStatus).ToList();

            foreach (var item in data) retorno.Add(new RequestBase(item, EmployeeDB.GetEmployeeById((int)item.userRequest), EmployeeDB.GetEmployeeById((int)item.responsableRequest)));

            return retorno;
        }

        

    }
}