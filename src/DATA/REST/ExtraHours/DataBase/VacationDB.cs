﻿using ExtraHours.Models;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExtraHours.DataBase
{
    public class VacationDB
    {
        public static List<VacationPeriod> GetAllByEmployee(int idEmployee)
        {
            var retorno = new List<VacationPeriod>();
            var db = Database.OpenNamedConnection("ExtraHours");

            var data = db.Vacaciones.All()
                                        .Where(db.Vacaciones.IDEmpleado == idEmployee).ToList();

            foreach (var item in data) retorno.Add(new VacationPeriod(item));

            return retorno;
            
        }
    }
}